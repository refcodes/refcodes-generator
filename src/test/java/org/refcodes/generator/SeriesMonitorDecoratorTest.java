// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.CharSet;

public class SeriesMonitorDecoratorTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////
	// private static boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean(
	// "test.log" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSeriesMonitorDecorator1() {
		final SeriesMonitorDecorator<String> theCounter = new SeriesMonitorDecorator<>( new AlphabetCounter( CharSet.DECIMAL ) );
		final String e1 = theCounter.next();
		final String e2 = theCounter.next();
		final String e3 = theCounter.next();
		theCounter.markAsProcessed( e1 );
		String eLast = theCounter.lastProcessed();
		assertEquals( e1, eLast );
		theCounter.markAsProcessed( e2 );
		eLast = theCounter.lastProcessed();
		assertEquals( e2, eLast );
		theCounter.markAsProcessed( e3 );
		eLast = theCounter.lastProcessed();
		assertEquals( e3, eLast );
		assertTrue( theCounter.isEmpty() );
	}

	@Test
	public void testSeriesMonitorDecorator2() {
		final SeriesMonitorDecorator<String> theCounter = new SeriesMonitorDecorator<>( new AlphabetCounter( CharSet.DECIMAL ) );
		final String e1 = theCounter.next();
		final String e2 = theCounter.next();
		final String e3 = theCounter.next();
		theCounter.markAsProcessed( e2 );
		String eLast = theCounter.lastProcessed();
		assertNull( eLast );
		theCounter.markAsProcessed( e1 );
		eLast = theCounter.lastProcessed();
		assertEquals( e2, eLast );
		theCounter.markAsProcessed( e3 );
		eLast = theCounter.lastProcessed();
		assertEquals( e3, eLast );
		assertTrue( theCounter.isEmpty() );
	}

	@Test
	public void testSeriesMonitorDecorator3() {
		final SeriesMonitorDecorator<String> theCounter = new SeriesMonitorDecorator<>( new AlphabetCounter( CharSet.DECIMAL ) );
		final String e1 = theCounter.next();
		final String e2 = theCounter.next();
		final String e3 = theCounter.next();
		theCounter.markAsProcessed( e3 );
		String eLast = theCounter.lastProcessed();
		assertNull( eLast );
		theCounter.markAsProcessed( e2 );
		eLast = theCounter.lastProcessed();
		assertNull( eLast );
		theCounter.markAsProcessed( e1 );
		eLast = theCounter.lastProcessed();
		assertEquals( e3, eLast );
		assertTrue( theCounter.isEmpty() );
	}
}
