// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;

public class AlphabetCounterMetricsTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testWordsFile1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "<'src/test/resources/wordlist.txt','src/test/resources/specialwords.txt'>" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theMetrics.toAlphabetExpression() );
		}
	}

	@Test
	@DisabledOnOs({ OS.WINDOWS, OS.OTHER })
	public void testWordsFile2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "<'src/test/resources/wordlist.txt','src/test/resources/specialwords.txt','/dev/stdin'>" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theMetrics.toAlphabetExpression() );
		}
	}

	@Test
	public void testWords1() throws ParseException {
		final String theExpression = "('Hello World','Mhoro Nyika!','HalloWelt','Ciao mondo!','Bonjour le monde!','Hola Mundo!','Chào thế giới!')";
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( theExpression );
		String theResult = Arrays.toString( theMetrics.getWords() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "[Hello World, Mhoro Nyika!, HalloWelt, Ciao mondo!, Bonjour le monde!, Hola Mundo!, Chào thế giới!]", theResult );
		theResult = theMetrics.toAlphabetExpression();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( theExpression, theResult );
	}

	@Test
	public void testWords2() throws ParseException {
		final String theExpression = "('Hello World',\"Mhoro Nyika!\",\"HalloWelt\",'Ciao mondo!','Bonjour le monde!','Hola Mundo!','Chào thế giới!')";
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( theExpression );
		String theResult = Arrays.toString( theMetrics.getWords() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "[Hello World, Mhoro Nyika!, HalloWelt, Ciao mondo!, Bonjour le monde!, Hola Mundo!, Chào thế giới!]", theResult );
		theResult = theMetrics.toAlphabetExpression();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( theExpression, theResult );
	}

	@Test
	public void testWords3() throws ParseException {
		try {
			new AlphabetCounterMetrics( "('Hello World',)" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 15, e.getErrorOffset() );
		}
	}

	@Test
	public void testWords4() throws ParseException {
		try {
			new AlphabetCounterMetrics( "('Hello World'," );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 1, e.getErrorOffset() );
		}
	}

	@Test
	public void testWords5() throws ParseException {
		final String theExpression = "('\"Hello World\"',\"'Mhoro Nyika!'\",\"'HalloWelt'\",'\"Ciao mondo!\"','\"Bonjour le monde!\"','\"Hola Mundo!\"','\"Chào thế giới!\"')";
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( theExpression );
		String theResult = Arrays.toString( theMetrics.getWords() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "[\"Hello World\", 'Mhoro Nyika!', 'HalloWelt', \"Ciao mondo!\", \"Bonjour le monde!\", \"Hola Mundo!\", \"Chào thế giới!\"]", theResult );
		theResult = theMetrics.toAlphabetExpression();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( theExpression, theResult );
	}

	@Test
	public void testWords6() throws ParseException {
		final String[] theWords = { "\"Hello World\"", "'Mhoro Nyika!'", "'HalloWelt'", "\"Ciao mondo!\"", "\"Bonjour le monde!\"", "\"Hola Mundo!\"", "\"Chào thế giới!\"" };
		AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( theWords );
		String theResult = Arrays.toString( theMetrics.getWords() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "[\"Hello World\", 'Mhoro Nyika!', 'HalloWelt', \"Ciao mondo!\", \"Bonjour le monde!\", \"Hola Mundo!\", \"Chào thế giới!\"]", theResult );
		theResult = theMetrics.toAlphabetExpression();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "('\"Hello World\"',\"'Mhoro Nyika!'\",\"'HalloWelt'\",'\"Ciao mondo!\"','\"Bonjour le monde!\"','\"Hola Mundo!\"','\"Chào thế giới!\"')", theResult );
		theMetrics = new AlphabetCounterMetrics( theResult );
		final String theOtherResult = theMetrics.toAlphabetExpression();
		assertEquals( theResult, theOtherResult );
	}

	@Test
	public void testWords7() throws ParseException {
		final String[] theWords = { "Hello World", "Mhoro Nyika!", "HalloWelt", "Ciao mondo!", "Bonjour le monde!", "Hola Mundo!", "Chào thế giới!" };
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( theWords );
		String theResult = Arrays.toString( theMetrics.getWords() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "[Hello World, Mhoro Nyika!, HalloWelt, Ciao mondo!, Bonjour le monde!, Hola Mundo!, Chào thế giới!]", theResult );
		theResult = theMetrics.toAlphabetExpression();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "(\"Hello World\",\"Mhoro Nyika!\",\"HalloWelt\",\"Ciao mondo!\",\"Bonjour le monde!\",\"Hola Mundo!\",\"Chào thế giới!\")", theResult );
	}

	@Test
	public void testWords8() throws ParseException, IOException {
		final String theExpression = "('this','is','my','word','list','to','use'):\"list\"";
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( theExpression );
		String theResult = Arrays.toString( theMetrics.getWords() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "[this, is, my, word, list, to, use]", theResult );
		theResult = theMetrics.toAlphabetExpression();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		final String[] theElements = { "list", "to", "use" };
		int index = 0;
		final AlphabetCounter theCounter = new AlphabetCounter( theMetrics );
		String eNext;
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theCounter.toAlphabetExpression() );
		}
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( theElements[index], eNext );
			index++;
		}
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theCounter.toAlphabetExpression() );
		}
		assertEquals( theElements.length, index );
		assertEquals( theExpression, theResult );
	}

	@Test
	public void testAlphabetExpression1() {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( 0, 12, "ABCDEFGHIJKLMNOPQRSTUVWXYZ\t".toCharArray() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theMetrics.toAlphabetExpression() );
		}
		assertEquals( "[0-12]:{A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,U+0009}", theMetrics.toAlphabetExpression() );
	}

	@Test
	public void testAlphabetExpression2() {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "STARTVALUE", 0, 12, "ABCDEFGHIJKLMNOPQRSTUVWXYZ\t".toCharArray() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theMetrics.toAlphabetExpression() );
		}
		assertEquals( "[0-12]:{A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,U+0009}:\"STARTVALUE\"", theMetrics.toAlphabetExpression() );
	}

	@Test
	public void testUnicodeRange1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{0-9,U+0041-U+005A}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", new String( theMetrics.getAlphabet() ) );
		assertEquals( null, theMetrics.getStartValue() );
	}

	@Test
	public void testUnicodeRange2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{U+0041-U+005A}:'ABC'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ", new String( theMetrics.getAlphabet() ) );
		assertEquals( "ABC", theMetrics.getStartValue() );
	}

	@Test
	public void testUnicodeRange3() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{U+0041-U+005A,a,b,c}:'ABC'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabc", new String( theMetrics.getAlphabet() ) );
		assertEquals( "ABC", theMetrics.getStartValue() );
	}

	@Test
	public void testUnicodeRange4() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{U+0041-U+005A,,}:'ABC'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ,", new String( theMetrics.getAlphabet() ) );
		assertEquals( "ABC", theMetrics.getStartValue() );
	}

	@Test
	public void testUnicodeRange5() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{U+0041-U+005A,,,a,b,c}:'ABC'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ,abc", new String( theMetrics.getAlphabet() ) );
		assertEquals( "ABC", theMetrics.getStartValue() );
	}

	@Test
	public void testAlphabet1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet3() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z,0-9}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet4() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z,0-9,,}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet5() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z,0-9,ä,ö,ü}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789äöü", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet6() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,,,;,a-z,0-9,ä,ö,ü}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ,;abcdefghijklmnopqrstuvwxyz0123456789äöü", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet7() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[3-12]:{!-?}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 3, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "!\"#$%&'()*+,-./0123456789:;<=>?", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testAlphabet8() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[3-12]:{!-?, }" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 3, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "!\"#$%&'()*+,-./0123456789:;<=>? ", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testReverseAlphabet1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{9-0,z-a,Z-A}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testReverseAlphabet2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{9-0,z-a,Z-A,,}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA,", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testParseException1() throws FileNotFoundException {
		try {
			new AlphabetCounterMetrics( "[0-3]:{A-Z,}" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 11, e.getErrorOffset() );
		}
	}

	@Test
	public void testParseException2() throws FileNotFoundException {
		try {
			new AlphabetCounterMetrics( "[0-3]:{A-Z,,x}" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 11, e.getErrorOffset() );
		}
	}

	@Test
	public void testCharSet1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[5-12]:{ASCII}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 5, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testCharSet2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[5-12]:{UPPER_CASE,LOWER_CASE}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 5, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testCharSet3() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[5-12]:{UPPER_CASE,LOWER_CASE,A-Z}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 5, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testEscapeCodes1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[1-3]:{\\t,\\b,\\n,\\r,\\f,\\\",\\',\\\\}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 1, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "\t\b\n\r\f\"\'\\", new String( theMetrics.getAlphabet() ) );
	}

	@Test
	public void testStartValueAlphabet1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z}:'HAL'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ", new String( theMetrics.getAlphabet() ) );
		assertEquals( "HAL", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z}:'Hal'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Hal", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet3() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z,0-9}:'Xy9'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Xy9", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet4() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z,0-9,,}:'Xy9'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Xy9", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet5() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,a-z,0-9,ä,ö,ü}:'äA0'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789äöü", new String( theMetrics.getAlphabet() ) );
		assertEquals( "äA0", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet6() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{A-Z,,,;,a-z,0-9,ä,ö,ü}:'ä,3'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZ,;abcdefghijklmnopqrstuvwxyz0123456789äöü", new String( theMetrics.getAlphabet() ) );
		assertEquals( "ä,3", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet7() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[3-12]:{!-?}:'!?!'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 3, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "!\"#$%&'()*+,-./0123456789:;<=>?", new String( theMetrics.getAlphabet() ) );
		assertEquals( "!?!", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueAlphabet8() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[3-12]:{!-?, }:'! ?'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 3, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "!\"#$%&'()*+,-./0123456789:;<=>? ", new String( theMetrics.getAlphabet() ) );
		assertEquals( "! ?", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueReverseAlphabet1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{9-0,z-a,Z-A}:'Za4'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Za4", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueReverseAlphabet2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-3]:{9-0,z-a,Z-A,,}:'Z,4'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 3, theMetrics.getMaxLength() );
		assertEquals( "9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA,", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Z,4", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueParseException1() throws FileNotFoundException {
		try {
			new AlphabetCounterMetrics( "[0-3]:{A-Z,}:'XYZ'" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 11, e.getErrorOffset() );
		}
	}

	@Test
	public void testStartValueParseException2() throws FileNotFoundException {
		try {
			new AlphabetCounterMetrics( "[0-3]:{A-Z,,x}:'XYZ'" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 11, e.getErrorOffset() );
		}
	}

	@Test
	public void testStartValueCharSet1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[5-12]:{ASCII}:'Hallo'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 5, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Hallo", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueCharSet2() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[5-12]:{UPPER_CASE,LOWER_CASE}:'Hallo'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 5, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Hallo", theMetrics.getStartValue() );
	}

	@Test
	public void testStartValueCharSet3() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[5-12]:{UPPER_CASE,LOWER_CASE,A-Z}:'Hallo'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 5, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", new String( theMetrics.getAlphabet() ) );
		assertEquals( "Hallo", theMetrics.getStartValue() );
	}

	@Test
	public void testEdgeCase1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{a,b,c,U+03B1,DECIMAL,X-Z,,}:',αb3Y'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "abcα0123456789XYZ,", new String( theMetrics.getAlphabet() ) );
		assertEquals( ",αb3Y", theMetrics.getStartValue() );
	}

	@Test
	public void testEdgeCase2() throws ParseException {
		try {
			new AlphabetCounterMetrics( "[0-12]:{a,b,c,U+03B1,DECIMAL,X-Z,}:'αb3Y'" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 33, e.getErrorOffset() );
		}
	}

	@Test
	public void testEdgeCase3() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{a,b,c,,,U+03B1,DECIMAL,X-Z}:',αb3Y'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "abc,α0123456789XYZ", new String( theMetrics.getAlphabet() ) );
		assertEquals( ",αb3Y", theMetrics.getStartValue() );
	}

	@Test
	public void testEdgeCase4() throws ParseException {
		try {
			new AlphabetCounterMetrics( "[0-12]:{a,b,c,,U+03B1,DECIMAL,X-Z}:',αb3Y'" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 14, e.getErrorOffset() );
		}
	}

	@Test
	public void testEdgeCase5() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{a,b,c,{,},DECIMAL,X-Z,,}:',{}b3Y'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "abc{}0123456789XYZ,", new String( theMetrics.getAlphabet() ) );
		assertEquals( ",{}b3Y", theMetrics.getStartValue() );
	}

	@Test
	public void testEdgeCase6() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{a,b,c,U+03B1,DECIMAL,X-Z,,}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "abcα0123456789XYZ,", new String( theMetrics.getAlphabet() ) );
		assertEquals( null, theMetrics.getStartValue() );
	}

	@Test
	public void testEdgeCase7() throws ParseException {
		try {
			new AlphabetCounterMetrics( "[0-12]:{a,b,c,U+03B1,DECIMAL,X-Z,}" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 33, e.getErrorOffset() );
		}
	}

	@Test
	public void testEdgeCase8() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{a,b,c,,,U+03B1,DECIMAL,X-Z}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "abc,α0123456789XYZ", new String( theMetrics.getAlphabet() ) );
		assertEquals( null, theMetrics.getStartValue() );
	}

	@Test
	public void testEdgeCase9() throws ParseException {
		try {
			new AlphabetCounterMetrics( "[0-12]:{a,b,c,,U+03B1,DECIMAL,X-Z}" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 14, e.getErrorOffset() );
		}
	}

	@Test
	public void testEdgeCase10() throws ParseException {
		try {
			new AlphabetCounterMetrics( "[0-12]:{a,b,c,U+03B1,DECIMAL,X-Z}:" );
			fail( "Expected a <" + ParseException.class.getSimpleName() + ">!" );
		}
		catch ( ParseException e ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expected <" + e.getClass().getSimpleName() + "> at position <" + e.getErrorOffset() + ">: " + e.getMessage() );
			}
			assertEquals( 33, e.getErrorOffset() );
		}
	}

	@Test
	public void testEdgeCase11() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "[0-12]:{a,b,c,{,},DECIMAL,X-Z,,}" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
			System.out.println( "Alphabet = + " + new String( theMetrics.getAlphabet() ) );
		}
		assertEquals( 0, theMetrics.getMinLength() );
		assertEquals( 12, theMetrics.getMaxLength() );
		assertEquals( "abc{}0123456789XYZ,", new String( theMetrics.getAlphabet() ) );
		assertEquals( null, theMetrics.getStartValue() );
	}

	@Test
	public void testDummy() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "'Hallo'" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min length = " + theMetrics.getMinLength() );
			System.out.println( "Max length = " + theMetrics.getMaxLength() );
			System.out.println( "Start value = " + theMetrics.getStartValue() );
		}
		assertEquals( "Hallo", theMetrics.getStartValue() );
	}
}
