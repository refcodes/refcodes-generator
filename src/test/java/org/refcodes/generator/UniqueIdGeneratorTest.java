package org.refcodes.generator;

import static org.junit.jupiter.api.Assertions.*;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 * The Class UniqueIdGeneratorTest.
 */
public class UniqueIdGeneratorTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );
	private static final int ITERATIONS = 1000;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNextIdString() {
		final Set<String> theSet = new HashSet<>();
		for ( int i = 0; i < ITERATIONS; i++ ) {
			final String nextIdString = UniqueIdGeneratorSingleton.getInstance().next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( nextIdString );
			}
			theSet.add( nextIdString );
		}
		assertEquals( ITERATIONS, theSet.size() );
	}

	@Test
	public void testNextId6String() {
		final UniqueIdGenerator theGenerator = new UniqueIdGenerator( 6 );
		final Set<String> theSet = new HashSet<>();
		for ( int i = 0; i < ITERATIONS; i++ ) {
			final String nextIdString = theGenerator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( nextIdString );
			}
			theSet.add( nextIdString );
		}
		assertEquals( ITERATIONS, theSet.size() );
	}

	@Test
	public void testNextId12String() {
		final UniqueIdGenerator theGenerator = new UniqueIdGenerator( 12 );
		final Set<String> theSet = new HashSet<>();
		for ( int i = 0; i < ITERATIONS; i++ ) {
			final String nextIdString = theGenerator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( nextIdString );
			}
			theSet.add( nextIdString );
		}
		assertEquals( ITERATIONS, theSet.size() );
	}

	@Test
	public void testNextId16String() {
		final UniqueIdGenerator theGenerator = new UniqueIdGenerator( 16 );
		final Set<String> theSet = new HashSet<>();
		for ( int i = 0; i < ITERATIONS; i++ ) {
			final String nextIdString = theGenerator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( nextIdString );
			}
			theSet.add( nextIdString );
		}
		assertEquals( ITERATIONS, theSet.size() );
	}

	@Test
	public void testNextId24String() {
		final UniqueIdGenerator theGenerator = new UniqueIdGenerator( 24 );
		final Set<String> theSet = new HashSet<>();
		for ( int i = 0; i < ITERATIONS; i++ ) {
			final String nextIdString = theGenerator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( nextIdString );
			}
			theSet.add( nextIdString );
		}
		assertEquals( ITERATIONS, theSet.size() );
	}

	@Test
	public void testNextId32String() {
		final UniqueIdGenerator theGenerator = new UniqueIdGenerator( 32 );
		final Set<String> theSet = new HashSet<>();
		for ( int i = 0; i < ITERATIONS; i++ ) {
			final String nextIdString = theGenerator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( nextIdString );
			}
			theSet.add( nextIdString );
		}
		assertEquals( ITERATIONS, theSet.size() );
	}
}
