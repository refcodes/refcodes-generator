// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.io.FileNotFoundException;
import java.text.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class BufferedGeneratorTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("Just for debugging purposes")
	@Test
	public void testConcurrentBufferedGenerator() throws ParseException, FileNotFoundException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "'HalloWelt'", "[0-32]:{0-9,A-Z,a-z}" );
		final BufferedGenerator<String> theDecorator = new ConcurrentBufferedGeneratorDecorator<>( theCounter );
		int index = 0;
		String eNext;
		while ( theDecorator.hasNext() ) {
			eNext = theDecorator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + index + "]: " + eNext );
			}
			index++;
		}
	}

	@Disabled("Just for debugging purposes")
	@Test
	public void testThreadLocalBufferedGeneratorDecorator() throws ParseException, FileNotFoundException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "'HalloWelt'", "[0-32]:{0-9,A-Z,a-z}" );
		final BufferedGenerator<String> theDecorator = new ThreadLocalBufferedGeneratorDecorator<>( theCounter );
		int index = 0;
		String eNext;
		while ( theDecorator.hasNext() ) {
			eNext = theDecorator.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + index + "]: " + eNext );
			}
			index++;
		}
	}
}
