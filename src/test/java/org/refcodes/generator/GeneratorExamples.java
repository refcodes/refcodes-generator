// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.io.FileNotFoundException;
import java.text.ParseException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.CharSet;

@Disabled("Enable me in order to run the examples!")
public class GeneratorExamples {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPasswords1() {
		final AlphabetCounter theCounter = new AlphabetCounter( 2, 4, 'a', 'b', 'c', 'd', 'e', 'f' );
		System.out.println( theCounter.toAlphabetExpression() );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
		System.out.println( theCounter.toAlphabetExpression() );
	}

	@Test
	public void testPasswords2() {
		final AlphabetCounter theCounter = new AlphabetCounter( 2, 4, CharSet.HEXADECIMAL );
		System.out.println( theCounter.toAlphabetExpression() );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
		System.out.println( theCounter.toAlphabetExpression() );
	}

	@Test
	public void testPasswords3() {
		final AlphabetCounter theCounter = new AlphabetCounter( new String[] { "this", "is", "my", "word", "list", "to", "iterate", "through" } );
		System.out.println( theCounter.toAlphabetExpression() );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
		System.out.println( theCounter.toAlphabetExpression() );
	}

	@Test
	public void testPasswords4() throws ParseException, FileNotFoundException {
		final AlphabetCounter theCounter = new AlphabetCounter( "[2-4]:{a,b,c,d,e,f}:'ce'" );
		System.out.println( theCounter.toAlphabetExpression() );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
		System.out.println( theCounter.toAlphabetExpression() );
	}

	@Test
	public void testPasswords5() throws ParseException, FileNotFoundException {
		final AlphabetCounter theCounter = new AlphabetCounter( "[2-4]:{HEXADECIMAL}:'CE'" );
		System.out.println( theCounter.toAlphabetExpression() );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
		System.out.println( theCounter.toAlphabetExpression() );
	}

	@Test
	public void testPasswords6() throws ParseException, FileNotFoundException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "('Secret','secret')", "[0-3]:{0-9}", "('','Password','password')", "[0-3]:{!,?}" );
		int count = 0;
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
			count++;
		}
		System.out.println( count );
	}

	@Test
	public void testPasswords7() throws ParseException, FileNotFoundException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "[1-3]:{A-Z}", "[3-3]:{0-9}" );
		int count = 0;
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
			count++;
		}
		System.out.println( count );
	}

	@Test
	public void testPasswords8() throws ParseException, FileNotFoundException {
		final AlphabetCounter theCounter1 = new AlphabetCounter( "[1-3]:{A-Z}" );
		final AlphabetCounter theCounter2 = new AlphabetCounter( "[3-3]:{0-9}" );
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( theCounter1, theCounter2 );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
	}

	@Test
	public void testPasswords9() throws ParseException {
		final AlphabetCounter theCounter1 = new AlphabetCounter( 1, 3, CharSet.UPPER_CASE );
		final AlphabetCounter theCounter2 = new AlphabetCounter( 3, 3, CharSet.DECIMAL );
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( theCounter1, theCounter2 );
		while ( theCounter.hasNext() ) {
			System.out.println( theCounter.next() );
		}
	}

	@Test
	public void testUniqueIds1() {
		for ( int i = 0; i < 1000; i++ ) {
			System.out.println( UniqueIdGeneratorSingleton.getInstance().next() );
		}
	}

	@Test
	public void testUniqueIds2() {
		final UniqueIdGenerator theGenerator = new UniqueIdGenerator( 16 );
		for ( int i = 0; i < 1000; i++ ) {
			System.out.println( theGenerator.next() );
		}
	}

	@Test
	public void testBufferedGenerator1() throws ParseException, FileNotFoundException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "'HelloWorld'", "[0-32]:{0-9,A-Z,a-z}" );
		final BufferedGenerator<String> theGenerator = new ConcurrentBufferedGeneratorDecorator<>( theCounter );
		int index = 0;
		while ( theGenerator.hasNext() && index < 1000 ) {
			System.out.println( theGenerator.next() );
			index++;
		}
	}

	@Test
	public void testLocalBufferedGenerator2() throws ParseException, FileNotFoundException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "'HelloWorld'", "[0-32]:{0-9,A-Z,a-z}" );
		final BufferedGenerator<String> theGenerator = new ThreadLocalBufferedGeneratorDecorator<>( theCounter );
		int index = 0;
		while ( theGenerator.hasNext() && index < 1000 ) {
			System.out.println( theGenerator.next() );
			index++;
		}
	}
}
