// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.refcodes.data.CharSet;

public class AlphabetCounterTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEdgeCase() {
		final String[] theExpected = new String[] { "", "0", "1", "00", "01", "10", "11", "000", "001", "010", "011", "100", "101", "110", "111" };
		final AlphabetCounter theCounter = new AlphabetCounter( 0, 3, CharSet.BINARY );
		String eNext;
		int index = 0;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "\"" + eNext + "\"" );
			}
			assertEquals( theExpected[index], eNext );
			index++;
		}
	}

	@Test
	public void testWordsFile1() throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( "<'src/test/resources/wordlist.txt','src/test/resources/specialwords.txt'>" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theMetrics.toAlphabetExpression() );
		}
		final AlphabetCounter theCounter = new AlphabetCounter( theMetrics );
		var count = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( count + ": " + eNext );
			}
			count++;
		}
		assertEquals( 69906, count );
	}

	@Test
	public void testStartValue1() throws ParseException {
		final AlphabetCounter theCounter = new AlphabetCounter( new String[] { "Hallo" } );
		assertTrue( theCounter.hasNext() );
		String theValue = theCounter.next();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertFalse( theCounter.hasNext() );
		theCounter.reset();
		assertTrue( theCounter.hasNext() );
		theValue = theCounter.next();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertFalse( theCounter.hasNext() );
	}

	@Test
	public void testWords1() {
		final String[] theWords = { "Hello World!", "Mhoro Nyika!", "Hallo Welt!", "Ciao mondo!", "Bonjour le monde!", "Hola Mundo!", "Chào thế giới!" };
		final AlphabetCounter theCounter = new AlphabetCounter( theWords );
		assertEquals( 11, theCounter.getMinLength() );
		assertEquals( 17, theCounter.getMaxLength() );
		int i = 0;
		while ( theCounter.hasNext() ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			assertEquals( theWords[i], theNext );
			i++;
		}
		assertEquals( 7, i );
	}

	@Test
	public void testWords2() {
		final String[] theWords = { "Hello World!", "Mhoro Nyika!", "Hallo Welt!", "Ciao mondo!", "Bonjour le monde!", "Hola Mundo!", "Chào thế giới!" };
		final AlphabetCounter theCounter = new AlphabetCounter( theWords );
		assertEquals( 11, theCounter.getMinLength() );
		assertEquals( 17, theCounter.getMaxLength() );
		int i = 0;
		while ( theCounter.hasNext() ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			assertEquals( theWords[i], theNext );
			i++;
		}
		assertEquals( 7, i );
		theCounter.reset();
		i = 0;
		while ( theCounter.hasNext() ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			assertEquals( theWords[i], theNext );
			i++;
		}
		assertEquals( 7, i );
	}

	@Test
	public void testAlphabetCounterDec1() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( CharSet.DECIMAL );
		for ( int i = 0; i < 100; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 100, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterDec2() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( "1112", CharSet.DECIMAL );
		for ( int i = 0; i < 100; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
			assertEquals( Integer.toString( ( i + 1112 ) ), theNext );
		}
		assertEquals( 100, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterDec3() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( "1", CharSet.DECIMAL );
		for ( int i = 0; i < 100; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 100, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterDec4() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( 10, CharSet.DECIMAL );
		for ( int i = 0; i < 100; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
			assertEquals( i, Integer.valueOf( theNext ) );
			assertEquals( 10, theNext.length() );
		}
		assertEquals( 100, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterDec5() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( "0000001112", CharSet.DECIMAL, 10 );
		for ( int i = 0; i < 100; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
			assertEquals( ( i + 1112 ), Integer.valueOf( theNext ) );
			assertEquals( 10, theNext.length() );
		}
		assertEquals( 100, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterDec6() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( 3, CharSet.DECIMAL );
		for ( int i = 0; i < 1500; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
			assertTrue( theNext.length() >= 3 );
		}
		assertEquals( 1500, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterDec7() {
		try {
			new AlphabetCounter( "100!", CharSet.DECIMAL );
			fail( "Expected an <" + IllegalArgumentException.class.getSimpleName() + "> exception!" );
		}
		catch ( IllegalArgumentException expected ) {}
	}

	@Test
	public void testAlphabetCounterBin1() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( CharSet.BINARY );
		for ( int i = 0; i < 255; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 255, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterBin2() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( "1111111100000000", CharSet.BINARY );
		for ( int i = 0; i < 255; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 255, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterBin3() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( 3, 6, CharSet.BINARY ); // Bin digits 3, 4, 5, 6 ~ 8 + 16 + 32 + 64 = 120
		for ( int i = 0; i < 255; i++ ) {
			try {
				final String theNext = theCounter.next();
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( theNext + " #[" + i + "]" );
				}
				if ( !theUniques.add( theNext ) ) {
					fail( "The value <" + theNext + "> is duplicate!" );
				}
				assertTrue( theNext.length() >= 3 );
				assertTrue( theNext.length() <= 6 );
			}
			catch ( IllegalStateException expected ) {
				if ( i != 120 ) {
					fail( "Expected an <" + IllegalStateException.class.getName() + "> after upon <64>th iterations, not after actual iterations of <" + i + ">!" );
				}
				break;
			}
		}
		assertEquals( 120, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterBin4() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( 3, 6, CharSet.BINARY ); // Bin digits 3, 4, 5, 6 ~ 8 + 16 + 32 + 64 = 120
		int i = 0;
		while ( theCounter.hasNext() ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
			assertTrue( theNext.length() >= 3 );
			assertTrue( theNext.length() <= 6 );
			i++;
		}
		assertEquals( 120, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterBin5() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( 6, 6, CharSet.BINARY );
		int i = 0;
		while ( theCounter.hasNext() ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
			assertTrue( theNext.length() >= 3 );
			assertTrue( theNext.length() <= 6 );
			i++;
		}
		assertEquals( 64, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterBin6() {
		try {
			new AlphabetCounter( "1002", CharSet.BINARY );
			fail( "Expected an <" + IllegalArgumentException.class.getSimpleName() + "> exception!" );
		}
		catch ( IllegalArgumentException expected ) {}
	}

	@Test
	public void testAlphabetCounterAsc1() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( CharSet.ASCII );
		for ( int i = 0; i < 255; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 255, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterAsc2() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( "Secret123!", CharSet.ASCII );
		for ( int i = 0; i < 255; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 255, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterAsc3() {
		final Set<String> theUniques = new HashSet<>();
		final AlphabetCounter theCounter = new AlphabetCounter( "Secret123!", CharSet.ASCII, 10 );
		for ( int i = 0; i < 255; i++ ) {
			final String theNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theNext + " #[" + i + "]" );
			}
			if ( !theUniques.add( theNext ) ) {
				fail( "The value <" + theNext + "> is duplicate!" );
			}
		}
		assertEquals( 255, theUniques.size() );
	}

	@Test
	public void testAlphabetCounterAsc4() {
		try {
			new AlphabetCounter( "Secret😉123", CharSet.ASCII );
			fail( "Expected an <" + IllegalArgumentException.class.getSimpleName() + "> exception!" );
		}
		catch ( IllegalArgumentException expected ) {}
	}
}
