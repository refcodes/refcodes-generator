// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class IdCounterCompositeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCounters1() {
		final String[] theExpected = new String[] { "0AA", "0AB", "0BA", "0BB", "1AA", "1AB", "1BA", "1BB" };
		final AlphabetCounter theCounter1 = new AlphabetCounter( 1, 1, '0', '1' );
		final AlphabetCounter theCounter2 = new AlphabetCounter( 2, 2, 'A', 'B' );
		final IdCounterComposite theCounter = new IdCounterComposite( theCounter1, theCounter2 );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( theExpected[index], eNext );
			index++;
		}
	}

	@Test
	public void testCounters2() {
		final String[] theExpected = new String[] { "0AAX", "0AAY", "0ABX", "0ABY", "0BAX", "0BAY", "0BBX", "0BBY", "1AAX", "1AAY", "1ABX", "1ABY", "1BAX", "1BAY", "1BBX", "1BBY" };
		final AlphabetCounter theCounter1 = new AlphabetCounter( 1, 1, '0', '1' );
		final AlphabetCounter theCounter2 = new AlphabetCounter( 2, 2, 'A', 'B' );
		final AlphabetCounter theCounter3 = new AlphabetCounter( 1, 1, 'X', 'Y' );
		final IdCounterComposite theCounter = new IdCounterComposite( theCounter1, theCounter2, theCounter3 );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( theExpected[index], eNext );
			index++;
		}
	}
}
