// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import org.junit.jupiter.api.Test;

public class AlphabetCounterCompositeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEdgeCase() throws ParseException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "[1-1]:{S,s}", "'yntax'", "[1-1]:{E,e}", "'rror'", "[1-1]:{A,a}", "'t'", "[1-1]:{L,l}", "'ine'", "[0-4]:{0-9}:''" );
		String eNext = null;
		String ePrev = null;
		int index = 0;
		while ( theCounter.hasNext() ) {
			ePrev = eNext;
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertNotEquals( ePrev, eNext );
			index++;
		}
		assertEquals( 177776, index );
	}

	@Test
	public void testWordsFileComposite1() throws ParseException {
		final String[] theExpected = new String[] { "special", "special0", "special1", "special2", "special3", "special4", "special5", "special6", "special7", "special8", "special9", "eigen", "eigen0", "eigen1", "eigen2", "eigen3", "eigen4", "eigen5", "eigen6", "eigen7", "eigen8", "eigen9", "kutonhora", "kutonhora0", "kutonhora1", "kutonhora2", "kutonhora3", "kutonhora4", "kutonhora5", "kutonhora6", "kutonhora7", "kutonhora8", "kutonhora9" };
		final AlphabetCounterMetrics theMetrics1 = new AlphabetCounterMetrics( "<'src/test/resources/specialwords.txt'>" );
		final AlphabetCounterMetrics theMetrics2 = new AlphabetCounterMetrics( "[0-1]:{0-9}" );
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( theMetrics1, theMetrics2 );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( theExpected[index], eNext );
			index++;
		}
		assertEquals( 33, index );
	}

	@Test
	public void testWordsFileComposite2() throws ParseException {
		final String[] theExpected = new String[] { "special", "eigen", "kutonhora", "0special", "0eigen", "0kutonhora", "1special", "1eigen", "1kutonhora", "2special", "2eigen", "2kutonhora", "3special", "3eigen", "3kutonhora", "4special", "4eigen", "4kutonhora", "5special", "5eigen", "5kutonhora", "6special", "6eigen", "6kutonhora", "7special", "7eigen", "7kutonhora", "8special", "8eigen", "8kutonhora", "9special", "9eigen", "9kutonhora" };
		final AlphabetCounterMetrics theMetrics2 = new AlphabetCounterMetrics( "<'src/test/resources/specialwords.txt'>" );
		final AlphabetCounterMetrics theMetrics1 = new AlphabetCounterMetrics( "[0-1]:{0-9}" );
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( theMetrics1, theMetrics2 );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( theExpected[index], eNext );
			index++;
		}
		assertEquals( 33, index );
	}

	@Test
	public void testWordsFileComposite3() throws ParseException {
		final AlphabetCounterMetrics theMetrics0 = new AlphabetCounterMetrics( "[2-2]:{A-C}" );
		final AlphabetCounterMetrics theMetrics1 = new AlphabetCounterMetrics( "<'src/test/resources/specialwords.txt'>" );
		final AlphabetCounterMetrics theMetrics2 = new AlphabetCounterMetrics( "[0-1]:{0-9}" );
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( theMetrics0, theMetrics1, theMetrics2 );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			index++;
		}
		System.out.println( index );
	}

	@Test
	public void testAlphabetCounterExpression1() throws ParseException {
		final String[] theExpected = new String[] { "XA", "XB", "XC", "X0A", "X0B", "X0C", "X1A", "X1B", "X1C", "X2A", "X2B", "X2C", "X3A", "X3B", "X3C", "X4A", "X4B", "X4C", "X5A", "X5B", "X5C", "X6A", "X6B", "X6C", "X7A", "X7B", "X7C", "X8A", "X8B", "X8C", "X9A", "X9B", "X9C", "YA", "YB", "YC", "Y0A", "Y0B", "Y0C", "Y1A", "Y1B", "Y1C", "Y2A", "Y2B", "Y2C", "Y3A", "Y3B", "Y3C", "Y4A", "Y4B", "Y4C", "Y5A", "Y5B", "Y5C", "Y6A", "Y6B", "Y6C", "Y7A", "Y7B", "Y7C", "Y8A", "Y8B", "Y8C", "Y9A", "Y9B", "Y9C", "ZA", "ZB", "ZC", "Z0A", "Z0B", "Z0C", "Z1A", "Z1B", "Z1C", "Z2A", "Z2B", "Z2C", "Z3A", "Z3B", "Z3C", "Z4A", "Z4B", "Z4C", "Z5A", "Z5B", "Z5C", "Z6A", "Z6B", "Z6C", "Z7A", "Z7B", "Z7C", "Z8A", "Z8B", "Z8C", "Z9A", "Z9B", "Z9C" };
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "[1-1]:{X-Z}", "[0-1]:{0-9}", "[1-1]:{A-C}" );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( theExpected[index], eNext );
			index++;
		}
	}

	@Test
	public void testAlphabetCounterExpression2() throws ParseException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "'HalloWelt'", "[2-2]:{0-9}" );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( index, Integer.parseInt( eNext.substring( 9 ) ) );
			assertEquals( "HalloWelt", eNext.substring( 0, 9 ) );
			index++;
		}
		assertEquals( 100, index );
	}

	@Test
	public void testAlphabetCounterExpression3() throws ParseException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "[2-2]:{0-9}", "'HalloWelt'" );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( index, Integer.parseInt( eNext.substring( 0, 2 ) ) );
			assertEquals( "HalloWelt", eNext.substring( 2 ) );
			index++;
		}
		assertEquals( 100, index );
	}

	@Test
	public void testAlphabetCounterExpression4() throws ParseException {
		final AlphabetCounterComposite theCounter = new AlphabetCounterComposite( "'Hallo'", "[2-2]:{0-9}", "'Welt'" );
		int index = 0;
		String eNext;
		while ( theCounter.hasNext() ) {
			eNext = theCounter.next();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eNext );
			}
			assertEquals( "Hallo", eNext.substring( 0, 5 ) );
			assertEquals( index, Integer.parseInt( eNext.substring( 5, 7 ) ) );
			assertEquals( "Welt", eNext.substring( 7 ) );
			index++;
		}
		assertEquals( 100, index );
	}
}
