module org.refcodes.generator {
	requires transitive org.refcodes.data;
	requires org.refcodes.numerical;
	requires java.logging;

	exports org.refcodes.generator;
}
