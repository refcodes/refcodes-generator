// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * The {@link SeriesMonitorDecorator} decorates a {@link Generator} with means
 * to monitor the last fully processed (completed) ID of the series of generated
 * IDs with all preceding generated IDs also being fully processed (completed):
 * All IDs till the last fully processed (completed) ID have been processed. Any
 * IDs being processed after this last fully processed (completed) ID are not
 * taken into account as between them and the the last fully processed
 * (completed) ID there are gaps of uncompleted (not yet finished) IDs (which
 * were not filled up yet e.g. as of a multi threaded environment).
 *
 * @param <T> the generic type of the elements being generated.
 */
public class SeriesMonitorDecorator<T> implements SeriesMonitor<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Generator<T> _decoratee;
	private final List<T> _generated = new ArrayList<>();
	private final List<Boolean> _processed = new ArrayList<>();
	private T _lastProcessed = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link SeriesMonitorDecorator} decorating the provided
	 * {@link Generator}.
	 * 
	 * @param aDecoratee The {@link Generator} to be decorated.
	 */
	public SeriesMonitorDecorator( Generator<T> aDecoratee ) {
		_decoratee = aDecoratee;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		_decoratee.remove();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return _decoratee.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		final T e = _decoratee.next();
		_generated.add( e );
		_processed.add( false );
		return e;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void forEachRemaining( Consumer<? super T> aAction ) {
		_decoratee.forEachRemaining( aAction );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markAsProcessed( T aId ) {
		final int index = _generated.indexOf( aId );
		if ( index != -1 ) {
			_processed.set( index, true );
		}
		final Iterator<Boolean> e = _processed.iterator();
		while ( e.hasNext() ) {
			if ( e.next() ) {
				_lastProcessed = _generated.remove( 0 );
				e.remove();
			}
			else {
				break;
			}
		}
		return index != -1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T lastProcessed() {
		return _lastProcessed;
	}

	/**
	 * Clears the list of unprocessed IDs.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		_generated.clear();
		_processed.clear();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Test interface method to test if all internally stored ID lists are
	 * empty.
	 * 
	 * @return True in case all internal lists are empty, ales false.
	 */
	boolean isEmpty() {
		return _generated.isEmpty() & _processed.isEmpty();
	}
}
