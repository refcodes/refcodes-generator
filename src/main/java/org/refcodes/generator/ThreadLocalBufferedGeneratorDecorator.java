// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.mixin.Disposable;

/**
 * The {@link ThreadLocalBufferedGeneratorDecorator} decorates a
 * {@link Generator} with read-ahead functionality by buffering generated IDs on
 * a per {@link Thread} basis: The resulting {@link Generator} is thread safe!.
 *
 * @param <T> the generic type of the elements being generated.
 */
public class ThreadLocalBufferedGeneratorDecorator<T> implements BufferedGenerator<T>, DecorateeAccessor<Iterator<T>>, Disposable {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final int DEFAULT_BUFFER_SIZE = 1024;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ThreadLocal<List<T>> _threadBuffer = new ThreadLocal<>();
	private Iterator<T> _decoratee;
	private int _bufferSize;
	private boolean _isSuspended = false;
	private boolean _isDisposed = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ThreadLocalBufferedGeneratorDecorator} decorating
	 * the provided {@link Generator} using the {@link #DEFAULT_BUFFER_SIZE} for
	 * the per thread read-ahead buffer.
	 * 
	 * @param aDecoratee The {@link Generator} to be decorated.
	 */
	public ThreadLocalBufferedGeneratorDecorator( Iterator<T> aDecoratee ) {
		this( aDecoratee, DEFAULT_BUFFER_SIZE );
	}

	/**
	 * Constructs the {@link ThreadLocalBufferedGeneratorDecorator} decorating
	 * the provided {@link Generator}.
	 * 
	 * @param aDecoratee The {@link Generator} to be decorated.
	 * 
	 * @param aBufferSize The size of the read-ahead buffer
	 */
	public ThreadLocalBufferedGeneratorDecorator( Iterator<T> aDecoratee, int aBufferSize ) {
		_decoratee = aDecoratee;
		_bufferSize = aBufferSize;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		final List<T> theLocalBuffer = getLocalBuffer();
		if ( _isDisposed ) {
			theLocalBuffer.clear();
		}
		return !theLocalBuffer.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		final List<T> theLocalBuffer = getLocalBuffer();
		if ( !theLocalBuffer.isEmpty() && !_isDisposed ) {
			return theLocalBuffer.remove( 0 );
		}
		else {
			if ( _isDisposed ) {
				theLocalBuffer.clear();
			}
			throw new NoSuchElementException( "No more elements left in the underlying generator <" + _decoratee + ">!" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		final List<T> theLocalBuffer = getLocalBuffer();
		if ( !theLocalBuffer.isEmpty() ) {
			theLocalBuffer.remove( 0 );
		}
		else {
			_decoratee.remove();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void suspend() {
		_isSuspended = true;
	}

	/**
	 * {@inheritDoc}
	 */
	//	@Override
	//	public void restart() {
	//		_isSuspended = false;
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		_isDisposed = true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private List<T> getLocalBuffer() {
		List<T> theLocalBuffer = _threadBuffer.get();
		if ( theLocalBuffer == null || theLocalBuffer.isEmpty() ) {
			if ( theLocalBuffer == null ) {
				theLocalBuffer = new ArrayList<>();
				_threadBuffer.set( theLocalBuffer );
			}
			if ( !_isSuspended ) {
				synchronized ( this ) {
					for ( int i = 0; i < _bufferSize && _decoratee.hasNext(); i++ ) {
						theLocalBuffer.add( _decoratee.next() );
					}
				}
			}
		}
		return theLocalBuffer;
	}
}
