// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Base64.Encoder;

import org.refcodes.data.PaddingChar;
import org.refcodes.data.Text;
import org.refcodes.numerical.NumericalUtility;

/**
 * The {@link UniqueIdGenerator} is an implementation of the {@link Generator}
 * capable of generating TID {@link String} instances unique on the system on
 * which them were generated to IDs generated with the same
 * {@link UniqueIdGenerator} on another system.
 */
public class UniqueIdGenerator implements IdGenerator {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default length for a UUID, Java's UUID class returns strings with a
	 * length of 36.
	 */
	private static final int UUID_LENGTH = 36;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _idLength;
	private IdEncoding _idEncoding;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Defines the type of BASE-64 encoding to use for generated IDs.
	 */
	public enum IdEncoding {
		/**
		 * Normal BASE-64 alike encoding (padding chars are omitted).
		 */
		BASE64(Base64.getEncoder()),

		/**
		 * BASE64 URL alike encoding (padding chars are omitted) enabling the
		 * use of the IDs in an URL.
		 */
		BASE64_URL(Base64.getUrlEncoder());

		final Encoder encoder;

		private IdEncoding( Encoder aEncoder ) {
			encoder = aEncoder;
		}
	}

	/**
	 * A server in the Internet has its unique IP so that we do not get TID
	 * conflicts for different servers.
	 */
	private static final byte[] IP_ADDRESS = new byte[2];
	private static long COUNTER = 0;
	private static long SEED;

	static {
		initSeed();
	}

	/**
	 * This method initializes the TID generator, it is invoked upon loading the
	 * utility class by the class loader (static block), though it may be called
	 * manually in case of TID collisions. Then a new random number as starting
	 * point for a specific part of the TID and the IP address are newly set.
	 */
	public static void initSeed() {
		SEED = new SecureRandom().nextLong();
		try {
			final byte[] ipAddress = InetAddress.getLocalHost().getAddress();
			IP_ADDRESS[0] = ipAddress[ipAddress.length - 2];
			IP_ADDRESS[1] = ipAddress[ipAddress.length - 1];
		}
		catch ( UnknownHostException e ) {
			new SecureRandom().nextBytes( IP_ADDRESS );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link UniqueIdGenerator} with a default TID length of 36.
	 * Uses BASE-64 alike encoding (padding chars omitted).
	 */
	public UniqueIdGenerator() {
		this( UUID_LENGTH, IdEncoding.BASE64_URL );
	}

	/**
	 * Constructs a {@link UniqueIdGenerator} with the provided TID length. Uses
	 * BASE-64 alike encoding (padding chars omitted).
	 * 
	 * @param aIdLength The length for the generated IDs.
	 */
	public UniqueIdGenerator( int aIdLength ) {
		this( aIdLength, IdEncoding.BASE64_URL );
	}

	/**
	 * Constructs a {@link UniqueIdGenerator} with a default TID length of 36.
	 * 
	 * @param aIdEncoding The {@link IdEncoding} to use for the IDs being
	 *        generated (padding chars omitted).
	 */
	public UniqueIdGenerator( IdEncoding aIdEncoding ) {
		this( UUID_LENGTH, aIdEncoding );
	}

	/**
	 * Constructs a {@link UniqueIdGenerator} with the provided TID length.
	 * 
	 * @param aIdLength The length for the generated IDs.
	 * @param aIdEncoding The {@link IdEncoding} to use for the IDs being
	 *        generated (padding chars omitted).
	 */
	public UniqueIdGenerator( int aIdLength, IdEncoding aIdEncoding ) {
		_idLength = aIdLength;
		_idEncoding = aIdEncoding;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String next() {
		String thePrefix = null;
		thePrefix = _idEncoding.encoder.encodeToString( IP_ADDRESS );
		thePrefix = thePrefix.replaceAll( PaddingChar.BASE64.getChar() + "", "" );
		if ( _idLength <= thePrefix.length() ) {
			throw new IllegalStateException( "The desired length of <" + _idLength + "> is too short as the prefix \"" + thePrefix + "\" has already a length of <" + thePrefix.length() );
		}
		final Long theTime = System.currentTimeMillis();
		String theTimeString = null;
		theTimeString = _idEncoding.encoder.encodeToString( NumericalUtility.toBytes( theTime ) );
		theTimeString = theTimeString.replaceAll( PaddingChar.BASE64.getChar() + "", "" );
		String theSeedString = null;
		while ( theSeedString == null || theSeedString.isEmpty() ) {
			theSeedString = _idEncoding.encoder.encodeToString( NumericalUtility.toBytes( SEED++ ) );
			theSeedString = theSeedString.replaceAll( PaddingChar.BASE64.getChar() + "", "" );
			if ( theSeedString == null || theSeedString.isEmpty() ) {
				initSeed();
			}
		}

		// Shorten portions of the TID in case of very short TID lengths |-->
		if ( _idLength <= 4 ) {
			thePrefix = "";
		}
		int theSeedLength = 4;
		if ( _idLength <= 6 ) {
			theSeedLength -= ( 7 - _idLength );
		}
		// <--| Shorten portions of the TID in case of very short TID lengths

		if ( theSeedString.length() > theSeedLength ) {
			theSeedString = theSeedString.substring( theSeedString.length() - theSeedLength );
		}
		int theRemainingLength = _idLength - ( theSeedString.length() + thePrefix.length() );
		if ( theRemainingLength < 0 ) {
			throw new IllegalStateException( "Cannot create an TID of length <" + _idLength + "> as we are short of <" + Math.abs( theRemainingLength ) + "> digits; enlarge your length accordinbgly." );
		}

		// -----------------------------------
		// Everything fits without filling up:
		// -----------------------------------
		if ( theTimeString.length() >= theRemainingLength ) {
			final int beginIndex = theTimeString.length() - theRemainingLength;
			return thePrefix + theSeedString + theTimeString.substring( beginIndex );
		}

		final String theCounterHead = thePrefix + theSeedString + theTimeString;
		theRemainingLength -= theTimeString.length();

		// ------------------------------
		// We have to fill the string up:
		// ------------------------------
		String theCounterTail;
		theCounterTail = _idEncoding.encoder.encodeToString( NumericalUtility.toBytes( COUNTER++ ) );
		theCounterTail = theCounterTail.replaceAll( PaddingChar.BASE64.getChar() + "", "" );
		if ( theCounterTail.length() != theRemainingLength ) {
			if ( theCounterTail.length() > theRemainingLength ) {
				theCounterTail = theCounterTail.substring( 0, theRemainingLength );
			}
			else {
				final StringBuilder theBuffer = new StringBuilder();
				theBuffer.append( theCounterTail );
				for ( int i = 0; i < theRemainingLength - theCounterTail.length(); i++ ) {
					theBuffer.append( '0' );
				}
				theCounterTail = theBuffer.toString();
			}
		}

		return theCounterHead + theCounterTail;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Don't want a "refcodes-textual" dependency. Therefore the code is hacked
	 * (duplicated) here:
	 * 
	 * @see AlignTextUtility.toAlignLeft
	 */
	// @formatter:off
	// private static String toAlignLeft( String aText, int aLength, char aFillChar ) {
	//	if ( aText.length() == aLength ) { return aText; }
	//	if ( aText.length() > aLength ) { return aText.substring( 0, aLength ); }
	//	StringBuilder theBuffer = new StringBuilder();
	//	theBuffer.append( aText );
	//	for ( int i = 0; i < aLength - aText.length(); i++ ) {
	//		theBuffer.append( aFillChar );
	//	}
	//	return theBuffer.toString();
	// }
	// @formatter:on
}
