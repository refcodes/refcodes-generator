package org.refcodes.generator;

import java.text.ParseException;
import java.util.NoSuchElementException;

/**
 * The {@link AlphabetCounterComposite} creates a {@link IdCounterComposite}
 * composed of {@link AlphabetCounter} instances which's alphabets are retrieved
 * from expressions each (to be combined to a single {@link IdCounter}).
 */
public class AlphabetCounterComposite extends IdCounterComposite {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AlphabetCounterComposite} by evaluating the
	 * provided expressions and creating the according {@link AlphabetCounter}
	 * instances.
	 *
	 * @param aAlphabetExpressions the alphabet expressions from which to
	 *        construct the {@link AlphabetCounterMetrics} definitions for the
	 *        underlying {@link AlphabetCounter} instances.
	 * 
	 * @throws ParseException thrown in case the alphabet expression cannot be
	 *         parsed to an alphabet.
	 */
	public AlphabetCounterComposite( String... aAlphabetExpressions ) throws ParseException {
		super( toAlphabetCounters( aAlphabetExpressions ) );
	}

	/**
	 * Constructs the {@link AlphabetCounterComposite} from the provided
	 * {@link AlphabetCounterMetrics} definitions.
	 *
	 * @param aAlphabetCounterMetrics The {@link AlphabetCounterMetrics}
	 *        definitions from which to construct the underlying
	 *        {@link AlphabetCounter} instances.
	 */
	public AlphabetCounterComposite( AlphabetCounterMetrics... aAlphabetCounterMetrics ) {
		super( toAlphabetCounters( aAlphabetCounterMetrics ) );
	}

	/**
	 * Constructs the {@link AlphabetCounterComposite} from the provided
	 * {@link AlphabetCounter} instances.
	 *
	 * @param aAlphabetCounter The {@link AlphabetCounter} instances to use.
	 */
	public AlphabetCounterComposite( AlphabetCounter... aAlphabetCounter ) {
		super( aAlphabetCounter );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the {@link #next()} value as of the individual underlying
	 * {@link AlphabetCounter} instances.
	 * 
	 * @return The according array with the indivudal next values.
	 * 
	 * @throws NoSuchElementException in case no more elements can be generated
	 *         by the encapsulated counters.
	 */
	@Override
	public String[] nextCounters() {
		return super.nextCounters();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AlphabetCounter[] getChildren() {
		final Counter<String>[] theCounters = super.getChildren();
		final AlphabetCounter[] theAlphabetCounters = new AlphabetCounter[theCounters.length];
		for ( int i = 0; i < theAlphabetCounters.length; i++ ) {
			theAlphabetCounters[i] = (AlphabetCounter) theCounters[i];
		}
		return theAlphabetCounters;
	}

	/**
	 * Provides the alphabet expressions as of the state of the nested
	 * {@link AlphabetCounter} instances.
	 * 
	 * @return The according alphabet expressions.
	 */
	public String[] toAlphabetExpressions() {
		final AlphabetCounter[] theCounters = getChildren();
		final String[] theExpressions = new String[theCounters.length];
		for ( int i = 0; i < theExpressions.length; i++ ) {
			theExpressions[i] = theCounters[i].toAlphabetExpression();
		}
		return theExpressions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		String theResult = "";
		final AlphabetCounter[] theCounters = getChildren();
		for ( AlphabetCounter eCounter : theCounters ) {
			theResult += eCounter.toString();
		}
		return theResult;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static IdCounter[] toAlphabetCounters( String[] aAlphabetExpressions ) throws ParseException {
		final IdCounter[] theCounters = new IdCounter[aAlphabetExpressions.length];
		for ( int i = 0; i < aAlphabetExpressions.length; i++ ) {
			theCounters[i] = new AlphabetCounter( new AlphabetCounterMetrics( aAlphabetExpressions[i] ) );
		}
		return theCounters;
	}

	private static IdCounter[] toAlphabetCounters( AlphabetCounterMetrics... aAlphabetCounterMetrics ) {
		final IdCounter[] theCounters = new IdCounter[aAlphabetCounterMetrics.length];
		for ( int i = 0; i < aAlphabetCounterMetrics.length; i++ ) {
			theCounters[i] = new AlphabetCounter( aAlphabetCounterMetrics[i] );
		}
		return theCounters;
	}
}
