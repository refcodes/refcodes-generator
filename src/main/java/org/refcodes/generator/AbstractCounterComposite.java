package org.refcodes.generator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.refcodes.mixin.ChildrenAccessor;

/**
 * The {@link AbstractCounterComposite} combines several {@link Counter}
 * instances which to use when generating data by daisy chaining the provided
 * generator instances (end of counter 1 increases counter 2, end of counter 2
 * increases counter 3, ..., end of counter n-1 increases counter n, the end of
 * a counter is determined with {@link #hasNext()} and a counter is increased by
 * {@link #next()}).
 * 
 * @param <T> the generic type of the elements being generated.
 */
public abstract class AbstractCounterComposite<T> implements Counter<T>, ChildrenAccessor<Counter<T>[]> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Counter<T>[] _counters;
	private T[] _actualCounters = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link AbstractCounterComposite} daisy chaining the provided
	 * {@link Counter} instances.
	 * 
	 * @param aCounters The {@link Counter} instances to be daisy chained.
	 */
	@SafeVarargs
	public AbstractCounterComposite( Counter<T>... aCounters ) {
		if ( aCounters == null || aCounters.length == 0 ) {
			throw new IllegalArgumentException( "You must provide at least one counter, though you provided " + ( aCounters != null ? "an emty array" : "<null>" ) + "!" );
		}
		_counters = aCounters;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		for ( int i = _counters.length - 1; i >= 0; i-- ) {
			if ( _counters[i].hasNext() ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		for ( Counter<T> eCounter : _counters ) {
			eCounter.reset();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Counter<T>[] getChildren() {
		return _counters;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the current actual counters' state (an individual value for each
	 * counter in the returned array as of the order the counters have been
	 * added).
	 * 
	 * @return The current actual counters' state.
	 */
	protected T[] actualCounters() {
		return _actualCounters;
	}

	/**
	 * Returns the {@link #next()} value as of the individual underlying
	 * {@link AlphabetCounter} instances (an individual value for each counter
	 * in the returned array as of the order the counters have been added).
	 * 
	 * @return The according array with the counters' individual next values.
	 * 
	 * @throws NoSuchElementException in case no more elements can be generated
	 *         by the encapsulated counters.
	 */
	@SuppressWarnings("unchecked")
	protected T[] nextCounters() {
		if ( _actualCounters == null ) {
			final List<T> theInitCount = new ArrayList<>();
			for ( int i = _counters.length - 1; i >= 0; i-- ) {
				theInitCount.add( _counters[_counters.length - 1 - i].next() );
			}
			_actualCounters = (T[]) Array.newInstance( theInitCount.get( 0 ).getClass(), theInitCount.size() );
			for ( int i = 0; i < _actualCounters.length; i++ ) {
				_actualCounters[i] = theInitCount.get( i );
			}
		}
		else {
			out: {
				for ( int i = _counters.length - 1; i >= 0; i-- ) {
					if ( _counters[i].hasNext() ) {
						_actualCounters[i] = _counters[i].next();
						break out;
					}
					_counters[i].reset();
					_actualCounters[i] = _counters[i].next();
				}
				throw new NoSuchElementException( "The counter reached already its last elemment < " + Arrays.toString( _actualCounters ) + ">!" );
			}
		}
		return _actualCounters;
	}
}
