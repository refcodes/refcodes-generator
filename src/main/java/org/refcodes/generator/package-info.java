// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact provides word morphs mass production functionality for
 * generating arbitrary distinct IDs similar to UUIDs (as of the
 * {@link org.refcodes.generator.UniqueIdGenerator} type) or for generating
 * permutations of words with a given alphabet and permutation rules (as of the
 * {@link org.refcodes.generator.AlphabetCounter} alongside the
 * {@link org.refcodes.generator.AlphabetCounterComposite} types) as well as
 * multi threading concurrency support (as of the
 * {@link org.refcodes.generator.ConcurrentBufferedGeneratorDecorator} and the
 * {@link org.refcodes.generator.ThreadLocalBufferedGeneratorDecorator} types)
 * for such {@link org.refcodes.generator.Generator}
 * ({@link java.util.Iterator}) types.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-generator"><strong>refcodes-generator:
 * Mass produce password permutations and distinct IDs</strong></a>
 * documentation for an up-to-date and detailed description on the usage of this
 * artifact.
 * </p>
 */
package org.refcodes.generator;