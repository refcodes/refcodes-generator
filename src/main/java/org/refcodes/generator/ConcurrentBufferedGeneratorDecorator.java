// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.DaemonLoopSleepTime;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.mixin.Disposable;

/**
 * The {@link ConcurrentBufferedGeneratorDecorator} decorates a
 * {@link Generator} with read-ahead functionality by buffering generated IDs
 * till a predefined maximum is reached and refilling the buffer periodically
 * upon dropping below a threshold. A background thread checks the buffer
 * periodically and takes care of refilling. The background task is paused upon
 * calling #suspend() and the resources are freed upon calling
 * {@link #dispose()}. The resulting {@link Generator} is thread safe!
 *
 * @param <T> the generic type of the elements being generated.
 */
public class ConcurrentBufferedGeneratorDecorator<T> implements BufferedGenerator<T>, DecorateeAccessor<Iterator<T>>, Disposable {

	private static final int POLL_QUEUE_LOOP_TIME_MILLIS = SleepLoopTime.MIN.getTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( ConcurrentBufferedGeneratorDecorator.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DEFAULT_MAX_BUFFER_SIZE = 1048576;
	private static final int DEFAULT_BUFFER_THRESHOLD = DEFAULT_MAX_BUFFER_SIZE / 2;
	private static final int DEFAULT_REFILL_LOOP_TIME_MILLIS = DaemonLoopSleepTime.MIN.getTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Iterator<T> _decoratee;
	private int _bufferThreshold;
	private int _maxBufferSize;
	private long _refillLoopTimeMillis;
	private BlockingQueue<T> _queue;
	private final ThreadLocal<T> _next = new ThreadLocal<>();
	private boolean _isDisposed = false;
	private boolean _isSuspended = false;
	private boolean _isExhausted = false;
	private boolean _isDaemon;
	private Timer _timer;
	private RefillTask _timerTask;
	private boolean _isAdaptive;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ConcurrentBufferedGeneratorDecorator} decorating
	 * the provided {@link Generator} using the {@link #DEFAULT_MAX_BUFFER_SIZE}
	 * for the per thread read-ahead buffer.
	 * 
	 * @param aDecoratee The {@link Generator} to be decorated.
	 */
	public ConcurrentBufferedGeneratorDecorator( Iterator<T> aDecoratee ) {
		this( aDecoratee, DEFAULT_MAX_BUFFER_SIZE, DEFAULT_BUFFER_THRESHOLD, DEFAULT_REFILL_LOOP_TIME_MILLIS, true, true );
	}

	/**
	 * Constructs the {@link ConcurrentBufferedGeneratorDecorator} decorating
	 * the provided {@link Generator} with the provided properties.
	 * 
	 * @param aDecoratee The {@link Generator} to be decorated.
	 * @param aMaxBufferSize The size of the read-ahead buffer
	 * @param aBufferThreshold The threshold at which the daemon is to begin
	 *        refilling the buffer till its buffer max size.
	 * @param aRefillLoopSleepTimeMillis The loop sleep time in milliseconds to
	 *        for the refill daemon to wait before testing (and refilling) the
	 *        buffer.
	 * @param isAdaptive True in case the refill loop sleep time is to be
	 *        adjusted in case after a sleep period the queue is empty.
	 * @param isDaemon True in case the daemon is to be started as daemon
	 *        thread.
	 */
	public ConcurrentBufferedGeneratorDecorator( Iterator<T> aDecoratee, int aMaxBufferSize, int aBufferThreshold, long aRefillLoopSleepTimeMillis, boolean isAdaptive, boolean isDaemon ) {
		_decoratee = aDecoratee;
		_bufferThreshold = aBufferThreshold;
		_maxBufferSize = aMaxBufferSize;
		_refillLoopTimeMillis = aRefillLoopSleepTimeMillis;
		_isAdaptive = isAdaptive;
		_queue = new LinkedBlockingQueue<>( aMaxBufferSize );
		_isDaemon = isDaemon;
		refill();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		if ( _next.get() != null ) {
			return true;
		}
		try {
			T theNext;
			do {
				theNext = _queue.poll( POLL_QUEUE_LOOP_TIME_MILLIS, TimeUnit.MILLISECONDS );
			} while ( theNext == null && ( !_isExhausted && !_isDisposed && !_isSuspended ) );
			if ( theNext == null ) {
				return false;
			}
			_next.set( theNext );
			return true;
		}
		catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		T theNext = _next.get();
		if ( theNext != null ) {
			_next.set( null );
			return theNext;
		}
		try {
			do {
				theNext = _queue.poll( POLL_QUEUE_LOOP_TIME_MILLIS, TimeUnit.MILLISECONDS );
				if ( theNext != null ) {
					return theNext;
				}
			} while ( theNext == null && ( !_isExhausted && !_isDisposed && !_isSuspended ) );
		}
		catch ( Exception e ) {
			throw new NoSuchElementException( "Cannot retrieve next element from buffer!", e );
		}
		throw new NoSuchElementException( "No more elements neither can be generated nor were found left in the queue!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void suspend() {
		_isSuspended = true;
		_timer.cancel();
	}

	/**
	 * {@inheritDoc}
	 */
	//	@Override
	//	public void restart() {
	//		if ( _isDisposed ) throw new IllegalStateException( "This instances has already been disposed!" );
	//		if ( !_isSuspended ) throw new IllegalStateException( "This instances not been suspended and is still proceeding!" );
	//		if ( _decoratee.hasNext() ) {
	//			_timerTask = new RefillTask();
	//			_timer = new Timer( _isDaemon );
	//			_timer.schedule( _timerTask, _refillLoopTimeMillis );
	//		}
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void dispose() {
		_isDisposed = true;
		_isExhausted = true;
		_isSuspended = true;
		_timer.cancel();
		_queue.clear();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void refill() {
		if ( !_isDisposed && !_isSuspended ) {

			if ( _queue.isEmpty() && _timer != null ) {
				LOGGER.log( Level.WARNING, "Queue of <" + ConcurrentBufferedGeneratorDecorator.class.getSimpleName() + "> is running dry (empty) with refill loop time <" + _refillLoopTimeMillis + "> ms, a max buffer size <" + _maxBufferSize + "> and a threshold of <" + _bufferThreshold + ">!" + ( _isAdaptive ? " (adjusting refill loop time to <" + ( _refillLoopTimeMillis - _refillLoopTimeMillis / 3 ) + "> ms)" : "" ) );
			}

			if ( _isAdaptive && _queue.isEmpty() ) {
				_refillLoopTimeMillis = _refillLoopTimeMillis - _refillLoopTimeMillis / 3;
			}
			if ( _queue.size() < _bufferThreshold ) {
				out: {
					while ( _decoratee.hasNext() && _queue.size() < _maxBufferSize && !_isSuspended && !_isDisposed ) {
						try {
							_queue.put( _decoratee.next() );
						}
						catch ( InterruptedException e ) {
							if ( _isDisposed || _isSuspended ) {
								return;
							}
							break out;
						}
					}
				}
			}
			else {
				if ( _isAdaptive ) {
					_refillLoopTimeMillis = _refillLoopTimeMillis + _refillLoopTimeMillis / 3;
				}
			}
			if ( _decoratee.hasNext() ) {
				if ( !_isSuspended && !_isDisposed ) {
					_timerTask = new RefillTask();
					_timer = new Timer( _isDaemon );
					_timer.schedule( _timerTask, _refillLoopTimeMillis );
				}
			}
			else {
				_isExhausted = true;
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class RefillTask extends TimerTask {

		@Override
		public void run() {
			refill();
		}
	}
}
