// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.refcodes.data.CharSet;

/**
 * The {@link AlphabetCounter} generates values by counting with an an alphabet
 * of "digits", a decimal alphabet would contain the characters "0", "1", "2",
 * "3", "4", "5", "6", "7", "8" and "9", a hexadecimal alphabet would contain
 * the characters "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B",
 * "C", "D", "E", "F".
 */
public class AlphabetCounter extends AlphabetCounterMetrics implements IdCounter {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _base = -1;
	private int[] _counter;
	private final Map<Character, Integer> _indexOf = new HashMap<>();
	private final Map<Integer, Character> _charAt = new HashMap<>();
	private int _nextWordIndex = 0;
	private int _readerIndex = 0;
	private String _nextReaderWord = null;
	private String _currentReaderWord = null;
	private BufferedReader[] _wordReaders = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AlphabetCounter} using the given words {@link File}
	 * elements to provide one by one (one word per line) contained in each file
	 * subsequently till all words have been processed, therewith representing
	 * the alphabet counter through which to iterate.
	 *
	 * @param aWordsFiles the words files
	 * 
	 * @throws IOException if an I/O error occurs while retrieving the
	 *         {@link FileDescriptor}.
	 */
	public AlphabetCounter( File... aWordsFiles ) throws IOException {
		this( null, aWordsFiles );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given words {@link File}
	 * elements to provide one by one (one word per line) contained in each file
	 * subsequently till all words have been processed, therewith representing
	 * the alphabet counter through which to iterate.
	 *
	 * @param aStartValue The value from which to start (to be contained in one
	 *        of the provided word {@link File} lists).
	 * @param aWordsFiles the words files
	 * 
	 * @throws IOException if an I/O error occurs when accessing the word
	 *         {@link File} lists.
	 */
	public AlphabetCounter( String aStartValue, File... aWordsFiles ) throws IOException {
		init( aStartValue, aWordsFiles );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given words
	 * {@link InputStream} elements to provide one by one (one word per line)
	 * contained in each file subsequently till all words have been processed,
	 * therewith representing the alphabet counter through which to iterate.
	 * 
	 * @param aWordsStreams The words {@link InputStream} lists representing the
	 *        alphabet counter through which to iterate.
	 * 
	 * @throws IOException if an I/O error occurs while retrieving the
	 *         {@link FileDescriptor}.
	 */
	public AlphabetCounter( InputStream... aWordsStreams ) throws IOException {
		this( null, aWordsStreams );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given words
	 * {@link InputStream} elements to provide one by one (one word per line)
	 * contained in each file subsequently till all words have been processed,
	 * therewith representing the alphabet counter through which to iterate.
	 * 
	 * @param aStartValue The value from which to start (to be contained in one
	 *        of the provided word {@link File} lists).
	 * 
	 * @param aWordsStreams The words {@link InputStream} lists representing the
	 *        alphabet counter through which to iterate.
	 * 
	 * @throws IOException if an I/O error occurs while retrieving the
	 *         {@link FileDescriptor}.
	 */
	public AlphabetCounter( String aStartValue, InputStream... aWordsStreams ) throws IOException {
		init( aStartValue, null, null, aWordsStreams, 0, -1, null );
	}

	/**
	 * Constructs an {@link AlphabetCounter} by parsing the given expression to
	 * construct the alphabet from the according expression's attributes.
	 *
	 * @param aExpression The expression to be parsed (duplicate definitions of
	 *        characters are ignored).
	 * 
	 * @throws ParseException thrown in case parsing the expression failed.
	 */
	public AlphabetCounter( String aExpression ) throws ParseException {
		final AlphabetCounterMetrics theMetrics = new AlphabetCounterMetrics( aExpression );
		init( theMetrics.getStartValue(), theMetrics.getWords(), theMetrics.getWordsFiles(), theMetrics.getWordsStreams(), theMetrics.getMinLength(), theMetrics.getMaxLength(), theMetrics.getAlphabet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given words which to
	 * provide one by one after all words have been processed..
	 * 
	 * @param aWords The words through which to iterate.
	 */
	public AlphabetCounter( String... aWords ) {
		this( null, aWords );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given words which to
	 * provide one by one after all words have been processed..
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * 
	 * @param aWords The words through which to iterate.
	 */
	public AlphabetCounter( String aStartValue, String... aWords ) {
		super( aStartValue, aWords );
		if ( aStartValue != null ) {
			_nextWordIndex = Arrays.asList( aWords ).indexOf( aStartValue );
		}
		_base = _words.length;
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( char... aAlphabet ) {
		this( Character.toString( aAlphabet[0] ), aAlphabet );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( Collection<Character> aAlphabet ) {
		this( toCharArray( aAlphabet ) );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( CharSet aAlphabet ) {
		this( aAlphabet.getCharSet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, Collection<Character> aAlphabet ) {
		this( aStartValue, toCharArray( aAlphabet ) );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, CharSet aAlphabet ) {
		this( aStartValue, aAlphabet.getCharSet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, char... aAlphabet ) {
		this( aStartValue, 1, aAlphabet );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( int aMinLength, char... aAlphabet ) {
		this( null, aMinLength, -1, aAlphabet );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( int aMinLength, Collection<Character> aAlphabet ) {
		this( null, aMinLength, -1, toCharArray( aAlphabet ) );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( int aMinLength, CharSet aAlphabet ) {
		this( null, aMinLength, -1, aAlphabet.getCharSet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 */
	public AlphabetCounter( String aStartValue, Collection<Character> aAlphabet, int aMinLength ) {
		this( aStartValue, aMinLength, -1, toCharArray( aAlphabet ) );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 */
	public AlphabetCounter( String aStartValue, CharSet aAlphabet, int aMinLength ) {
		this( aStartValue, aMinLength, -1, aAlphabet.getCharSet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, int aMinLength, char... aAlphabet ) {
		this( aStartValue, aMinLength, -1, aAlphabet );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link #hasNext()} method will return true till the maximum
	 *        length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( int aMinLength, int aMaxLength, char... aAlphabet ) {
		this( null, aMinLength, aMaxLength, aAlphabet );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link #hasNext()} method will return true till the maximum
	 *        length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( int aMinLength, int aMaxLength, Collection<Character> aAlphabet ) {
		this( null, aMinLength, aMaxLength, toCharArray( aAlphabet ) );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link #hasNext()} method will return true till the maximum
	 *        length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( int aMinLength, int aMaxLength, CharSet aAlphabet ) {
		this( null, aMinLength, aMaxLength, aAlphabet.getCharSet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link #hasNext()} method will return true till the maximum
	 *        length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, int aMinLength, int aMaxLength, Collection<Character> aAlphabet ) {
		this( aStartValue, aMinLength, aMaxLength, toCharArray( aAlphabet ) );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link #hasNext()} method will return true till the maximum
	 *        length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, int aMinLength, int aMaxLength, CharSet aAlphabet ) {
		this( aStartValue, aMinLength, aMaxLength, aAlphabet.getCharSet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given
	 * {@link AlphabetCounterMetrics}.
	 *
	 * @param aAlphabetCounterMetrics The {@link AlphabetCounterMetrics} to use
	 *        (alphabet, min length, max length).
	 */
	public AlphabetCounter( AlphabetCounterMetrics aAlphabetCounterMetrics ) {
		init( aAlphabetCounterMetrics.getStartValue(), aAlphabetCounterMetrics.getWords(), aAlphabetCounterMetrics.getWordsFiles(), aAlphabetCounterMetrics.getWordsStreams(), aAlphabetCounterMetrics.getMinLength(), aAlphabetCounterMetrics.getMaxLength(), aAlphabetCounterMetrics.getAlphabet() );
	}

	/**
	 * Constructs an {@link AlphabetCounter} using the given attributes.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link #hasNext()} method will return true till the maximum
	 *        length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounter( String aStartValue, int aMinLength, int aMaxLength, char... aAlphabet ) {
		init( aStartValue, null, null, null, aMinLength, aMaxLength, aAlphabet );
	}

	// -------------------------------------------------------------------------

	private void init( String aStartValue, File[] aWordsFiles ) throws FileNotFoundException {
		_startValue = aStartValue;
		_wordsFiles = aWordsFiles;

		if ( aWordsFiles != null && aWordsFiles.length != 0 ) {
			for ( int i = 0; i < aWordsFiles.length; i++ ) {
				_wordsStreams[i] = new FileInputStream( aWordsFiles[i] );
				_wordReaders[i] = new BufferedReader( new InputStreamReader( _wordsStreams[i] ) );
			}
		}
		seekStartPosition( aStartValue );
	}

	private void init( String aStartValue, String[] aWords, File[] aWordsFiles, InputStream[] aWordsStreams, int aMinLength, int aMaxLength, char... aAlphabet ) {
		if ( aWords == null && aWordsStreams == null && ( aAlphabet == null || aAlphabet.length == 0 ) && ( aStartValue == null || aStartValue.length() != aMinLength || aStartValue.length() != aMaxLength ) ) {
			throw new IllegalArgumentException( "The provided alphabet <" + ( aAlphabet != null ? Arrays.toString( aAlphabet ) : null ) + "> must at least contain one character!" );
		}
		if ( aMinLength < 0 ) {
			throw new IllegalArgumentException( "The provided min length <" + aMinLength + "> must be greater or equal to <0>!" );
		}
		_alphabet = aAlphabet;
		_minLength = aMinLength;
		_maxLength = aMaxLength;
		_words = aWords;
		if ( ( ( aStartValue == null || aStartValue.isEmpty() ) && aWords == null ) ) {
			aStartValue = "";
			for ( int i = 0; i < aMinLength; i++ ) {
				aStartValue += Character.toString( aAlphabet[0] );
			}
		}
		_startValue = aStartValue;
		if ( aWords != null && aStartValue != null ) {
			validateStartValue( aStartValue, aWords );
			_nextWordIndex = Arrays.asList( aWords ).indexOf( aStartValue );
		}
		if ( aAlphabet != null ) {
			for ( int i = 0; i < aAlphabet.length; i++ ) {
				_indexOf.put( aAlphabet[i], i );
				_charAt.put( i, aAlphabet[i] );
			}
			AlphabetCounterMetrics.validateStartValue( aStartValue, aMinLength, aMaxLength, aAlphabet );
		}
		if ( aWordsFiles != null && aWordsFiles.length != 0 ) {
			_wordsFiles = aWordsFiles;
		}
		if ( aWordsStreams != null && aWordsStreams.length != 0 ) {
			_wordsStreams = aWordsStreams;
			_wordReaders = new BufferedReader[aWordsStreams.length];
			for ( int i = 0; i < aWordsStreams.length; i++ ) {
				_wordReaders[i] = new BufferedReader( new InputStreamReader( aWordsStreams[i] ) );
			}
			seekStartPosition( aStartValue );
		}

		if ( aAlphabet != null ) {
			_base = aAlphabet.length;
			_counter = new int[_startValue.length()];
			for ( int i = 0; i < _counter.length; i++ ) {
				_counter[i] = _indexOf.get( _startValue.charAt( i ) );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		if ( _wordReaders != null ) {
			if ( _nextReaderWord != null ) {
				return true;
			}
			if ( _readerIndex >= _wordReaders.length ) {
				return false;
			}
			BufferedReader theReader = _wordReaders[_readerIndex];
			try {
				_nextReaderWord = theReader.readLine();
			}
			catch ( IOException ignore ) {}
			if ( _nextReaderWord == null ) {
				_readerIndex++;
				if ( _readerIndex >= _wordReaders.length ) {
					return false;
				}
				theReader = _wordReaders[_readerIndex];
				try {
					_nextReaderWord = theReader.readLine();
				}
				catch ( IOException ignore ) {}
			}
			_currentReaderWord = _nextReaderWord;
			return _nextReaderWord != null;
		}
		if ( _words != null ) {
			return ( _nextWordIndex < _words.length );
		}
		if ( _alphabet != null ) {
			if ( _maxLength == -1 || _nextWordIndex == 0 || _counter.length < _maxLength ) {
				return true;
			}
			for ( int a_counter : _counter ) {
				if ( a_counter < _base - 1 ) {
					return true;
				}
			}
			return false;
		}
		else {
			return _nextWordIndex == 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String next() {
		if ( !hasNext() ) {
			throw new IllegalStateException( "The next element would exceed the " + ( _words != null ? "total number of provided words <" + _words.length + ">" : "maximum specified length <" + _maxLength + ">" ) + "!" );
		}
		final String theNextReaderWord = _nextReaderWord;
		if ( theNextReaderWord != null ) {
			_nextReaderWord = null;
			return theNextReaderWord;
		}
		if ( _words != null ) {
			return _words[_nextWordIndex++];
		}
		if ( _nextWordIndex == 0 ) { // is it the first one?
			_nextWordIndex = -1; // next time not the first one any more!
			return toString();
		}
		for ( int i = _counter.length - 1; i >= 0; i-- ) {
			if ( _counter[i] < _base - 1 ) {
				_counter[i]++;
				return toString();
			}
			_counter[i] = 0;
		}
		if ( _maxLength == -1 || _counter.length < _maxLength ) {
			final int[] theCounter = Arrays.copyOf( _counter, _counter.length + 1 );
			theCounter[theCounter.length - 1] = 0;
			_counter = theCounter;
		}
		return toString();
	}

	/**
	 * Returns the alphabet used by this counter.
	 * 
	 * @return The according alphabet in use.
	 */
	@Override
	public char[] getAlphabet() {
		return _alphabet;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_nextWordIndex = 0;
		if ( _alphabet != null ) {
			final int[] theCounter = new int[_minLength];
			for ( int i = 0; i < theCounter.length; i++ ) {
				theCounter[i] = 0;
			}
			_counter = theCounter;
		}
		if ( _wordsStreams != null ) {
			_readerIndex = 0;
			_nextReaderWord = null;
			_currentReaderWord = null;
			for ( int i = 0; i < _wordsStreams.length; i++ ) {
				if ( _wordsStreams[i] instanceof FileInputStream eFileInputStream ) {
					try {
						eFileInputStream.getChannel().position( 0 );
					}
					catch ( IOException e ) {
						throw new UnsupportedOperationException( "The provided file input stream of type <" + _wordsStreams[i].getClass().getName() + "> " + ( _wordsFiles != null && _wordsFiles.length == _wordsStreams.length ? "for file <" + _wordsFiles[i].getAbsolutePath() + "> " : " " ) + "does not support reseting back to position <0>!", e );
					}
				}
				else {
					throw new UnsupportedOperationException( "The provided input stream of type <" + _wordsStreams[i].getClass() + "> does not support reseting!" );
				}
			}
			_wordReaders = new BufferedReader[_wordsStreams.length];
			for ( int i = 0; i < _wordsStreams.length; i++ ) {
				_wordReaders[i] = new BufferedReader( new InputStreamReader( _wordsStreams[i] ) ); // As we must cancel any buffered reads!
			}
		}
	}

	/**
	 * Provides the alphabet expressions as of the state of this
	 * {@link AlphabetCounter} instance.
	 * 
	 * @return The according alphabet expression.
	 */
	@Override
	public String toAlphabetExpression() {
		if ( _words != null ) {
			return toAlphabetExpression( toString(), _words );
		}
		if ( _wordsFiles != null ) {
			return toAlphabetExpression( toString(), _wordsFiles );
		}
		if ( _wordsStreams != null ) {
			throw new UnsupportedOperationException( "Cannot create an alphabet expression from streams with no (as far as can be seen) file references!" );
		}
		return toAlphabetExpression( toString(), _minLength, _maxLength, _alphabet );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		if ( _words != null ) {
			return _words[_nextWordIndex < _words.length ? _nextWordIndex : _words.length - 1];
		}
		if ( _wordsStreams != null ) {
			return _currentReaderWord;
		}
		if ( _alphabet == null ) {
			return _startValue;
		}
		final StringBuilder theBuffer = new StringBuilder();
		for ( int a_counter : _counter ) {
			theBuffer.append( _charAt.get( a_counter ) );
		}
		return theBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static char[] toCharArray( Collection<Character> aAlphabet ) {
		final char[] theAlphabet = new char[aAlphabet.size()];
		int index = 0;
		for ( char eChar : aAlphabet ) {
			theAlphabet[index] = eChar;
			index++;
		}
		return theAlphabet;
	}

	private void seekStartPosition( String aStartValue ) {
		if ( _wordsStreams != null && _wordsStreams.length != 0 ) {
			if ( aStartValue != null && aStartValue.length() != 0 ) {
				while ( hasNext() && !aStartValue.equals( _nextReaderWord ) ) {
					next(); // Skip the next one!
				}
			}
			//	else {
			//		hasNext(); // Load first word (required by "toString()")!
			//	}
		}
	}

	//	private InputStream[] toInputStreams( File[] aWordsFiles ) throws FileNotFoundException {
	//		InputStream[] theInputStreams = null;
	//		if ( aWordsFiles != null && aWordsFiles.length != 0 ) {
	//			theInputStreams = new InputStream[aWordsFiles.length];
	//			for ( int i = 0; i < aWordsFiles.length; i++ ) {
	//				theInputStreams[i] = new FileInputStream( aWordsFiles[i] );
	//			}
	//		}
	//		return theInputStreams;
	//	}
}
