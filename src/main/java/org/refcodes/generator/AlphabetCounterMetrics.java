// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.refcodes.data.CharSet;
import org.refcodes.data.EscapeCode;

/**
 * The {@link AlphabetCounterMetrics} define the metrics for configuring an
 * {@link AlphabetCounter} such as the characters used as alphabet and the
 * counter's min and max lengths (duplicate definitions of characters are
 * ignored).
 * 
 * The syntax for an expression is as follows:
 * <p>
 * <code>[minLength-maxLength]:{char(s)[,char(s),...,char(s)][:"{startValue}"</code>
 * <p>
 * or
 * <p>
 * <code>("word","word",...,"word")</code> or
 * <p>
 * or
 * <p>
 * <code>&lt;"/path/to/wordListFile","other/path/to/wordListFile"&gt;</code>
 * <ul>
 * <li><code>minLength</code>: The minimum length of the alphabet counter. Must
 * be provided and evaluate to an integer.
 * <li><code>maxLength</code>: The maximum length of the alphabet counter. Is
 * optional and must evaluate to an integer when provided. When -1 or not
 * provided denotes no max length limit.
 * <li><code>char(s)</code>: Either a single character, a range of characters
 * beginning with the first character of the range, followed by a "-" character
 * followed by the last character of the range (as of the UTF character values),
 * e.g. "A-Z" or "0-9" or "9-0" (for "reverse" order) or a {@link CharSet}
 * enumaertion's name. A comma is specified as any other single character.
 * <li><code>startValue</code>: The (optional) start value of the counter, when
 * omitted, then the start value is set to <code>null</code> and smallest start
 * value (as of min length, max length and the alphabet definition) is to be
 * used by consumers of the metrics.
 * <li><code>word</code>: The exact word(s) to be probed, useful in conjunction
 * with an {@link AlphabetCounterComposite} alongside with other
 * {@link AlphabetCounter} instances.
 * <li><code>wordListFile</code>: The word list file to be used for providing
 * words (one word per line) list by list, useful in conjunction with an
 * {@link AlphabetCounterComposite} alongside with other {@link AlphabetCounter}
 * instances.
 * </ul>
 * 
 * Examples:
 * 
 * <ul>
 * <li><code>[0-3]:{A-Z,a-z,0-9}</code>: Min length of the counter will be 0,
 * the max length 3, the alphabet contains all upper and lower case letters as
 * well as all the digits from 0 to 9.
 * <li><code>[0-3]:{A-Z,a-z,,,0-9}</code>: Same as above with an additional
 * comma (note that the comma is enclosed by two commas).
 * <li><code>[0-3]:{A-Z,a-z,0-9,,}</code>: Similar as above with the additional
 * comma (note that the comma is separated by a comma).
 * <li><code>[5-12]:{ASCII}</code>: Min length of the counter will be 5, the max
 * length 12, the alphabet contains the printable ASCII (7 bit) characters (as
 * of {@link CharSet#ASCII}).
 * <li><code>[5-12]:{ASCII,Ä,Ö,Ü,ä,ö,ü,ß}</code>: Same as above with the
 * additional characters "Ä", "Ö", "Ü", "ä", "ö", "ü" as well as "ß".
 * <li><code>[0-3]:{A-Z,a-z,0-9}:"my"</code>: Min length of the counter will be
 * 0, the max length 3, the alphabet contains all upper and lower case letters
 * as well as all the digits from 0 to 9, the start value is "my".
 * <li><code>("this",'is',"a","list","of","words","to","probe",'one',"after","the",'other')</code>:
 * This list of words is to be probed, excluding the opening and closing single
 * or the double quotation marks respectively (using single "'" quotation marks
 * you can use '"' double quotation marks in a word and vice versa).
 * </ul>
 */
public class AlphabetCounterMetrics {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final char LENGTH_SEPARATOR = '-';
	private static final char EXPRESSION_SEPARATOR = ',';
	private static final String SECTION_SEPARATOR = ":";
	private static final String LENGTH_BEGIN = "[";
	private static final String LENGTH_END = "]";
	private static final String ALPHABET_BEGIN = "{";
	private static final String ALPHABET_END = "}";
	private static final String WORD_BEGIN = "\"";
	private static final String WORD_END = "\"";
	private static final String ALT_WORD_BEGIN = "'";
	private static final String ALT_WORD_END = "'";
	private static final String WORDS_BEGIN = "(";
	private static final String WORDS_END = ")";
	private static final String WORDS_FILES_BEGIN = "<";
	private static final String WORDS_FILES_END = ">";
	private static final String UTF_CODEPOINT_PREFIX = "U+";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _startValue = null;
	protected String[] _words = null;
	protected char[] _alphabet = null;
	protected File[] _wordsFiles = null;
	protected InputStream[] _wordsStreams = null;
	protected int _minLength = 0;
	protected int _maxLength = -1;
	protected String _alphabetExpression;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new alphabet counter metrics.
	 */
	protected AlphabetCounterMetrics() {}

	/**
	 * Constructs an {@link AlphabetCounterMetrics} using the given words to
	 * provide one by one till all words have been processed, therewith
	 * representing the alphabet counter through which to iterate.
	 * 
	 * @param aWords The words representing the alphabet counter through which
	 *        to iterate.
	 */
	public AlphabetCounterMetrics( String... aWords ) {
		this( null, aWords );
	}

	/**
	 * Constructs an {@link AlphabetCounterMetrics} using the given words to
	 * provide one by one till all words have been processed, therewith
	 * representing the alphabet counter through which to iterate.
	 * 
	 * @param aStartValue The value from which to start (must be one of the
	 *        words provided).
	 * 
	 * @param aWords The words representing the alphabet counter through which
	 *        to iterate.
	 */
	public AlphabetCounterMetrics( String aStartValue, String[] aWords ) {
		if ( aWords == null | aWords.length == 0 ) {
			throw new IllegalArgumentException( "You must provide an array of words instead of " + ( aWords == null ? "<null>" : "an empty <{}> array!" ) );
		}
		validateStartValue( aStartValue, aWords );
		_startValue = aStartValue;
		_alphabetExpression = toAlphabetExpression( aStartValue, aWords );
		_words = aWords;
		initWords();
	}

	/**
	 * Constructs an {@link AlphabetCounterMetrics} using the given words
	 * {@link File} elements to provide one by one (one word per line) contained
	 * in each file subsequently till all words have been processed, therewith
	 * representing the alphabet counter through which to iterate.
	 *
	 * @param aWordsFiles the words files
	 * 
	 * @throws FileNotFoundException thrown in case a file provided was not
	 *         found. @ thrown in case a words file has been specified which was
	 *         not found.
	 */
	public AlphabetCounterMetrics( File... aWordsFiles ) throws FileNotFoundException {
		this( null, aWordsFiles );
	}

	/**
	 * Constructs an {@link AlphabetCounterMetrics} using the given words
	 * {@link File} elements to provide one by one (one word per line) contained
	 * in each file subsequently till all words have been processed, therewith
	 * representing the alphabet counter through which to iterate.
	 *
	 * @param aStartValue The value from which to start (to be contained in one
	 *        of the provided word {@link File} lists).
	 * @param aWordsFiles the words files
	 * 
	 * @throws FileNotFoundException thrown in case a file provided was not
	 *         found. @ thrown in case a words file has been specified which was
	 *         not found.
	 */
	public AlphabetCounterMetrics( String aStartValue, File... aWordsFiles ) throws FileNotFoundException {
		if ( aWordsFiles == null | aWordsFiles.length == 0 ) {
			throw new IllegalArgumentException( "You must provide an array of words files instead of " + ( aWordsFiles == null ? "<null>" : "an empty <{}> array!" ) );
		}
		_startValue = aStartValue;
		_alphabetExpression = toAlphabetExpression( aStartValue, aWordsFiles );
		_wordsFiles = aWordsFiles;
		if ( _wordsFiles != null && _wordsFiles.length != 0 ) {
			_wordsStreams = new FileInputStream[_wordsFiles.length];
			for ( int i = 0; i < _wordsFiles.length; i++ ) {
				_wordsStreams[i] = new FileInputStream( _wordsFiles[i] );
			}
		}
	}

	/**
	 * Parses the given expression to construct the
	 * {@link AlphabetCounterMetrics} from the according expression's
	 * attributes.
	 *
	 * @param aAlphabetExpression The expression to be parsed (duplicate
	 *        definitions of characters are ignored).
	 * 
	 * @throws ParseException thrown in case parsing the expression failed. @
	 *         thrown in case a words file has been specified which was not
	 *         found.
	 */
	public AlphabetCounterMetrics( String aAlphabetExpression ) throws ParseException {
		parseAlphabetExpression( aAlphabetExpression );
	}

	/**
	 * Constructs an {@link AlphabetCounterMetrics} using the given attributes.
	 * 
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link AlphabetCounter#hasNext()} method will return true till
	 *        the maximum length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounterMetrics( int aMinLength, int aMaxLength, char... aAlphabet ) {
		this( null, aMinLength, aMaxLength, aAlphabet );
	}

	/**
	 * Constructs an {@link AlphabetCounterMetrics} using the given attributes.
	 *
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link AlphabetCounter#hasNext()} method will return true till
	 *        the maximum length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 */
	public AlphabetCounterMetrics( String aStartValue, int aMinLength, int aMaxLength, char... aAlphabet ) {
		if ( aStartValue != null ) {
			validateStartValue( aStartValue, aMinLength, aMaxLength, aAlphabet );
		}
		_alphabet = aAlphabet;
		_minLength = aMinLength;
		_maxLength = aMaxLength;
		_startValue = aStartValue;
		_alphabetExpression = toAlphabetExpression( aStartValue, aMinLength, aMaxLength, aAlphabet );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the words to be used by the {@link AlphabetCounter}.
	 * 
	 * @return The according words.
	 */
	public String[] getWords() {
		return _words;
	}

	/**
	 * Retrieves the words {@link File} lists referenced by these
	 * {@link AlphabetCounterMetrics}.
	 * 
	 * @return The according words {@link File} lists.
	 */
	public File[] getWordsFiles() {
		return _wordsFiles;
	}

	/**
	 * Retrieves the words {@link InputStream} lists referenced by these
	 * {@link AlphabetCounterMetrics}.
	 * 
	 * @return The according words {@link InputStream} lists.
	 */
	public InputStream[] getWordsStreams() {
		return _wordsStreams;
	}

	/**
	 * Returns the alphabet to be used by the {@link AlphabetCounter}.
	 * 
	 * @return The according alphabet.
	 */
	public char[] getAlphabet() {
		return _alphabet;
	}

	/**
	 * Returns the counter's start value.
	 * 
	 * @return The according counter's start value.
	 */
	public String getStartValue() {
		return _startValue;
	}

	/**
	 * Returns the counter's min length.
	 * 
	 * @return The according counter's min length.
	 */
	public int getMinLength() {
		return _minLength;
	}

	/**
	 * Returns the counter's max length, a value of -1 denotes an infinite
	 * length.
	 * 
	 * @return The according counter's max length.
	 */
	public int getMaxLength() {
		return _maxLength;
	}

	/**
	 * Provides the alphabet expression represented by the
	 * {@link AlphabetCounterMetrics}.
	 * 
	 * @return The according alphabet expression.
	 */
	public String toAlphabetExpression() {
		return _alphabetExpression;
	}

	/**
	 * Constructs an alphabet expression from the provided words.
	 *
	 * @param aStartValue the start value
	 * @param aWords The words from which to construct the alphabet expression.
	 * 
	 * @return The according alphabet expression.
	 */
	public static String toAlphabetExpression( String aStartValue, String[] aWords ) {
		String theAlphabetExpression = WORDS_BEGIN;
		String eWordBegin;
		String eWordEnd;
		if ( aWords != null && aWords.length != 0 ) {
			for ( int i = 0; i < aWords.length; i++ ) {
				if ( aWords[i].contains( WORD_BEGIN ) ) {
					eWordBegin = ALT_WORD_BEGIN;
					eWordEnd = ALT_WORD_END;
				}
				else {
					eWordBegin = WORD_BEGIN;
					eWordEnd = WORD_END;
				}
				theAlphabetExpression += eWordBegin + aWords[i] + eWordEnd + ( i < aWords.length - 1 ? EXPRESSION_SEPARATOR : "" );
			}
		}
		theAlphabetExpression += WORDS_END;
		if ( aStartValue != null ) {
			theAlphabetExpression += theAlphabetExpression.isEmpty() ? WORD_BEGIN : SECTION_SEPARATOR + WORD_BEGIN;
			theAlphabetExpression += aStartValue + WORD_END;
		}
		return theAlphabetExpression;
	}

	/**
	 * Constructs an alphabet expression from the provided files.
	 *
	 * @param aStartValue the start value
	 * @param aWordsStreams The words {@link File} elements from which to
	 *        construct the alphabet expression.
	 * 
	 * @return The according alphabet expression.
	 * 
	 * @throws IOException if an I/O error occurs while retrieving the
	 *         {@link FileDescriptor}.
	 */
	public static String toAlphabetExpression( String aStartValue, InputStream[] aWordsStreams ) throws IOException {
		String theAlphabetExpression = WORDS_FILES_BEGIN;
		if ( aWordsStreams != null ) {
			if ( aWordsStreams.length == 1 ) {
				if ( aWordsStreams[0] instanceof FileInputStream theFileInput ) {
					if ( FileDescriptor.in.equals( theFileInput.getFD() ) ) {

					}
				}
			}
		}
		theAlphabetExpression += WORDS_FILES_END;
		if ( aStartValue != null ) {
			theAlphabetExpression += theAlphabetExpression.isEmpty() ? WORD_BEGIN : SECTION_SEPARATOR + WORD_BEGIN;
			theAlphabetExpression += aStartValue + WORD_END;
		}
		return theAlphabetExpression;
	}

	/**
	 * Constructs an alphabet expression from the provided files.
	 *
	 * @param aStartValue the start value
	 * @param aWordsFiles the words files
	 * 
	 * @return The according alphabet expression.
	 */
	public static String toAlphabetExpression( String aStartValue, File[] aWordsFiles ) {
		String theAlphabetExpression = WORDS_FILES_BEGIN;
		String eWordBegin;
		String eWordEnd;
		if ( aWordsFiles != null && aWordsFiles.length != 0 ) {
			for ( int i = 0; i < aWordsFiles.length; i++ ) {
				if ( aWordsFiles[i].getAbsolutePath().contains( WORD_BEGIN ) ) {
					eWordBegin = ALT_WORD_BEGIN;
					eWordEnd = ALT_WORD_END;
				}
				else {
					eWordBegin = WORD_BEGIN;
					eWordEnd = WORD_END;
				}
				theAlphabetExpression += eWordBegin + aWordsFiles[i].getAbsolutePath() + eWordEnd + ( i < aWordsFiles.length - 1 ? EXPRESSION_SEPARATOR : "" );
			}
		}
		theAlphabetExpression += WORDS_FILES_END;
		if ( aStartValue != null ) {
			theAlphabetExpression += theAlphabetExpression.isEmpty() ? WORD_BEGIN : SECTION_SEPARATOR + WORD_BEGIN;
			theAlphabetExpression += aStartValue + WORD_END;
		}
		return theAlphabetExpression;
	}

	/**
	 * Constructs an alphabet expression from the provided metrics.
	 * 
	 * @param aStartValue The value from which to start (must only consist of
	 *        characters found in the alphabet).
	 * @param aMinLength The minimum length of the counter value to be produced,
	 *        the counter value will be filled up with the first character of
	 *        the alphabet from left to right if it is shorter than the minimum
	 *        length.
	 * @param aMaxLength The maximum length of the counter value to be produced,
	 *        the {@link AlphabetCounter#hasNext()} method will return true till
	 *        the maximum length is exceed.
	 * @param aAlphabet The alphabet to use for counting, a decimal alphabet
	 *        would contain the characters "0", "1", "2", "3", "4", "5", "6",
	 *        "7", "8" and "9".
	 * 
	 * @return The according alphabet expression.
	 */
	public static String toAlphabetExpression( String aStartValue, int aMinLength, int aMaxLength, char... aAlphabet ) {
		String theAlphabetExpression = "";
		if ( aAlphabet != null && aAlphabet.length != 0 ) {
			theAlphabetExpression = aMinLength != -1 ? LENGTH_BEGIN + aMinLength + "" + LENGTH_SEPARATOR + "" + aMaxLength + LENGTH_END : "";
			theAlphabetExpression += theAlphabetExpression.isEmpty() ? ALPHABET_BEGIN : SECTION_SEPARATOR + ALPHABET_BEGIN;
			char eChar;
			String eHex;
			for ( int i = 0; i < aAlphabet.length; i++ ) {
				eChar = aAlphabet[i];
				if ( eChar < 32 || eChar == 127 ) {
					eHex = Integer.toHexString( eChar );
					while ( eHex.length() < 4 ) {
						eHex = "0" + eHex;
					}
					theAlphabetExpression += "U+" + eHex;
				}
				else {
					theAlphabetExpression += eChar;
				}
				if ( i < aAlphabet.length - 1 ) {
					theAlphabetExpression += EXPRESSION_SEPARATOR;
				}
			}
			theAlphabetExpression += ALPHABET_END;
		}
		if ( aStartValue != null && aStartValue.length() != 0 ) {
			theAlphabetExpression += theAlphabetExpression.isEmpty() ? WORD_BEGIN : SECTION_SEPARATOR + WORD_BEGIN;
			theAlphabetExpression += aStartValue + WORD_END;
		}
		return theAlphabetExpression;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return AlphabetCounterMetrics.toAlphabetExpression( _startValue, _minLength, _maxLength, _alphabet );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	static void validateStartValue( String aStartValue, int aMinLength, int aMaxLength, char... aAlphabet ) {
		if ( aStartValue.length() < aMinLength || ( aMaxLength != -1 && aStartValue.length() > aMaxLength ) ) {
			throw new IllegalArgumentException( "The length <" + aStartValue.length() + "> of the start value must be between <" + aMinLength + "> and <" + ( aMaxLength != -1 ? aMaxLength : "∞" ) + ">!" );
		}
		for ( int i = 0; i < aStartValue.length(); i++ ) {
			out: {
				for ( char anAAlphabet : aAlphabet ) {
					if ( aStartValue.charAt( i ) == anAAlphabet ) {
						break out;
					}
				}
				throw new IllegalArgumentException( "The provided start value \"" + aStartValue + "\" must only contain values found in the alphabet <" + Arrays.toString( aAlphabet ) + "> !" );
			}
		}
	}

	static void validateStartValue( String aStartValue, String[] aWords ) {
		if ( aStartValue != null ) {
			out: {
				for ( String eWord : aWords ) {
					if ( eWord.equals( aStartValue ) ) {
						break out;
					}
				}
				throw new IllegalArgumentException( "The start value \"" + aStartValue + "\" is not contained in the words list " + Arrays.toString( aWords ) + "!" );
			}
		}
	}

	/**
	 * Parses the provided alphabet expression and sets the properties of this
	 * {@link AlphabetCounterMetrics} instance accordingly.
	 * 
	 * @param aAlphabetExpression The alphabet expression to parse.
	 * 
	 * @throws ParseException thrown in case parsing the expression failed. @
	 *         thrown in case a words file has been specified which was not
	 *         found.
	 */
	protected void parseAlphabetExpression( String aAlphabetExpression ) throws ParseException {
		if ( aAlphabetExpression == null || aAlphabetExpression.isEmpty() ) {
			throw new IllegalArgumentException( "The alphabet expression must not be " + ( aAlphabetExpression == null ? "<null>" : "an empty string" ) + "!" );
		}

		final String theWordsSection = toSection( aAlphabetExpression, WORDS_BEGIN, WORDS_END, SECTION_SEPARATOR );
		final String theWordsFilesSection = toSection( aAlphabetExpression, WORDS_FILES_BEGIN, WORDS_FILES_END, SECTION_SEPARATOR );
		final String theLengthSection = toSection( aAlphabetExpression, LENGTH_BEGIN, LENGTH_END, SECTION_SEPARATOR );
		final String theAlphabetSection = toSection( aAlphabetExpression, ALPHABET_BEGIN, ALPHABET_END, SECTION_SEPARATOR );
		String theStartValueSection = toSection( aAlphabetExpression, WORD_BEGIN, WORD_END, SECTION_SEPARATOR );
		if ( theStartValueSection == null || theStartValueSection.isEmpty() ) {
			theStartValueSection = toSection( aAlphabetExpression, ALT_WORD_BEGIN, ALT_WORD_END, SECTION_SEPARATOR );
		}
		String theExpectedExpression = toAltAlphabetExpression( theLengthSection, theAlphabetSection, theStartValueSection, theWordsSection, theWordsFilesSection );
		if ( !theExpectedExpression.equals( aAlphabetExpression ) ) {
			theExpectedExpression = toAlphabetExpression( theLengthSection, theAlphabetSection, theStartValueSection, theWordsSection, theWordsFilesSection );
			if ( !theExpectedExpression.equals( aAlphabetExpression ) ) {
				for ( int i = 0; i < theExpectedExpression.length() && i < aAlphabetExpression.length(); i++ ) {
					if ( theExpectedExpression.charAt( i ) != aAlphabetExpression.charAt( i ) ) {
						throw new ParseException( "Unexpected character '" + aAlphabetExpression.charAt( i ) + "' at position <" + i + "> of the alphabet \"" + aAlphabetExpression + "\" (expected a '" + theExpectedExpression.charAt( i ) + "' character)!", i );
					}
				}
				if ( aAlphabetExpression.length() > theExpectedExpression.length() ) {
					throw new ParseException( "Unexpected '" + aAlphabetExpression.substring( theExpectedExpression.length() ) + "' beginning at position <" + theExpectedExpression.length() + "> of the alphabet \"" + aAlphabetExpression + "\"!", theExpectedExpression.length() );
				}

				throw new ParseException( "Expected '" + theExpectedExpression.substring( aAlphabetExpression.length() ) + "' at position <" + aAlphabetExpression.length() + "> of the alphabet \"" + aAlphabetExpression + "\"!", aAlphabetExpression.length() );
			}
		}
		if ( theLengthSection == null && theAlphabetSection == null && theStartValueSection == null && theWordsSection == null && theWordsFilesSection == null ) {
			throw new ParseException( "Encountered an invalid alphabet expression \"" + aAlphabetExpression + "\"!", 0 );
		}
		if ( theLengthSection == null && theAlphabetSection == null && theStartValueSection != null ) {
			_maxLength = theStartValueSection.length();
			_minLength = theStartValueSection.length();
		}
		if ( theLengthSection != null && theAlphabetSection == null ) {
			throw new ParseException( "No alphabet declared in alphabet expression \"" + aAlphabetExpression + "\"!", aAlphabetExpression.length() );
		}
		parseLength( theLengthSection, aAlphabetExpression );
		parseAlphabet( theAlphabetSection, aAlphabetExpression );
		parseStartValue( theStartValueSection, aAlphabetExpression );
		parseWords( theWordsSection, aAlphabetExpression );
		parseWordsFiles( theWordsFilesSection, aAlphabetExpression );
		if ( _startValue != null && _words != null ) {
			out: {
				for ( String eWord : _words ) {
					if ( eWord.equals( _startValue ) ) {
						break out;
					}
				}
				final int index = aAlphabetExpression.indexOf( theStartValueSection );
				throw new ParseException( "The start value \"" + _startValue + "\" is not contained in the words list " + Arrays.toString( _words ) + " at position <" + index + "> of the alphabet \"" + aAlphabetExpression + "\"!", index );
			}
		}
		if ( _wordsFiles != null && _wordsFiles.length != 0 ) {
			_alphabetExpression = toAlphabetExpression( _startValue, _wordsFiles ); // Absolute files!
		}
		else {
			_alphabetExpression = aAlphabetExpression;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void parseWordsFiles( String aWordsFilesDeclaration, String aAlphabetExpression ) throws ParseException {
		if ( aWordsFilesDeclaration != null ) {
			final int theOffset = aAlphabetExpression.indexOf( aWordsFilesDeclaration );
			final List<String> theWordsFiles = new ArrayList<>();
			int eDelimiterIndex = -1;
			do {
				String eWordEnd = WORD_END;
				int eStartIndex = aWordsFilesDeclaration.indexOf( WORD_BEGIN, eDelimiterIndex + 1 );
				if ( eStartIndex != eDelimiterIndex + 1 ) {
					eWordEnd = ALT_WORD_END;
					eStartIndex = aWordsFilesDeclaration.indexOf( ALT_WORD_BEGIN, eDelimiterIndex + 1 );
					if ( eStartIndex != eDelimiterIndex + 1 ) {
						throw new ParseException( "Expecting a '" + WORD_BEGIN + "' or a  a '" + ALT_WORD_BEGIN + "' symbol at position <" + ( theOffset + eDelimiterIndex + 1 ) + ">  for the words declaration \"" + aWordsFilesDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", ( theOffset + eDelimiterIndex + 1 ) );
					}
				}
				eStartIndex++;
				int eEndIndex = aWordsFilesDeclaration.indexOf( eWordEnd, eStartIndex );
				if ( eEndIndex == -1 ) {
					eEndIndex = aWordsFilesDeclaration.indexOf( eWordEnd, eStartIndex );
					if ( eEndIndex == -1 ) {
						throw new ParseException( "Expecting a '" + eWordEnd + "' symbol after  position <" + ( theOffset + eStartIndex ) + ">  for the words files declaration \"" + aWordsFilesDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", ( theOffset + eStartIndex ) );
					}
				}
				theWordsFiles.add( aWordsFilesDeclaration.substring( eStartIndex, eEndIndex ) );
				eDelimiterIndex = aWordsFilesDeclaration.indexOf( EXPRESSION_SEPARATOR, eEndIndex );
			} while ( eDelimiterIndex != -1 );

			final String[] theWordsFilePaths = theWordsFiles.toArray( new String[theWordsFiles.size()] );
			if ( theWordsFilePaths != null && theWordsFilePaths.length != 0 ) {
				_wordsFiles = new File[theWordsFilePaths.length];
				_wordsStreams = new InputStream[theWordsFilePaths.length];
				for ( int i = 0; i < theWordsFilePaths.length; i++ ) {
					_wordsFiles[i] = new File( theWordsFilePaths[i] );
					try {
						_wordsStreams[i] = new FileInputStream( _wordsFiles[i] );
					}
					catch ( FileNotFoundException e ) {
						throw new ParseException( "File <" + theWordsFilePaths[i] + "> from the words files declaration \"" + aWordsFilesDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\" not found!", aAlphabetExpression.indexOf( theWordsFilePaths[i] ) );
					}
				}
			}
		}
	}

	private void parseWords( String aWordsDeclaration, String aAlphabetExpression ) throws ParseException {
		if ( aWordsDeclaration != null ) {
			final int theOffset = aAlphabetExpression.indexOf( aWordsDeclaration );
			final List<String> theWords = new ArrayList<>();
			int eDelimiterIndex = -1;
			do {
				String eWordEnd = WORD_END;
				int eStartIndex = aWordsDeclaration.indexOf( WORD_BEGIN, eDelimiterIndex + 1 );
				if ( eStartIndex != eDelimiterIndex + 1 ) {
					eWordEnd = ALT_WORD_END;
					eStartIndex = aWordsDeclaration.indexOf( ALT_WORD_BEGIN, eDelimiterIndex + 1 );
					if ( eStartIndex != eDelimiterIndex + 1 ) {
						throw new ParseException( "Expecting a '" + WORD_BEGIN + "' or a  a '" + ALT_WORD_BEGIN + "' symbol at position <" + ( theOffset + eDelimiterIndex + 1 ) + ">  for the words declaration \"" + aWordsDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", ( theOffset + eDelimiterIndex + 1 ) );
					}
				}
				eStartIndex++;
				int eEndIndex = aWordsDeclaration.indexOf( eWordEnd, eStartIndex );
				if ( eEndIndex == -1 ) {
					eEndIndex = aWordsDeclaration.indexOf( eWordEnd, eStartIndex );
					if ( eEndIndex == -1 ) {
						throw new ParseException( "Expecting a '" + eWordEnd + "' symbol after  position <" + ( theOffset + eStartIndex ) + ">  for the words declaration \"" + aWordsDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", ( theOffset + eStartIndex ) );
					}
				}
				theWords.add( aWordsDeclaration.substring( eStartIndex, eEndIndex ) );
				eDelimiterIndex = aWordsDeclaration.indexOf( EXPRESSION_SEPARATOR, eEndIndex );
			} while ( eDelimiterIndex != -1 );
			_words = theWords.toArray( new String[theWords.size()] );
			initWords();
		}
	}

	private void initWords() {
		if ( _words != null ) {
			for ( String eWord : _words ) {
				if ( _minLength == 0 || _minLength > eWord.length() ) {
					_minLength = eWord.length();
				}
				if ( _maxLength < eWord.length() ) {
					_maxLength = eWord.length();
				}
			}
		}
	}

	private void parseLength( String aLengthDeclaration, String aAlphabetExpression ) throws ParseException {
		if ( aLengthDeclaration != null ) {
			final int theOffset = aAlphabetExpression.indexOf( aLengthDeclaration );
			final int theEndIndex = aLengthDeclaration.indexOf( LENGTH_SEPARATOR );
			if ( theEndIndex == -1 ) {
				throw new ParseException( "Expecting a '" + LENGTH_SEPARATOR + "' symbol after position <" + theOffset + "> for the lengths declaration \"" + aLengthDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", theOffset );
			}
			String eSubstring = aLengthDeclaration.substring( 0, theEndIndex );
			try {
				_minLength = Integer.parseInt( eSubstring );
			}
			catch ( NumberFormatException e ) {
				throw new ParseException( "Cannot parse min length from \"" + eSubstring + "\" at position <" + theOffset + "> for the lengths declaration \"" + aLengthDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", theOffset );
			}
			final int theStartIndex = theEndIndex + 1;
			eSubstring = aLengthDeclaration.substring( theStartIndex );
			try {
				_maxLength = Integer.parseInt( eSubstring );
			}
			catch ( NumberFormatException e ) {
				throw new ParseException( "Cannot parse max length from \"" + eSubstring + "\" at position <" + ( theOffset + theStartIndex ) + "> for the lengths declaration \"" + aLengthDeclaration + "\" of the alphabet \"" + aAlphabetExpression + "\"!", theOffset + theStartIndex );
			}
		}
	}

	private void parseAlphabet( String aAlphabetDeclaration, String aAlphabetExpression ) throws ParseException {
		if ( aAlphabetDeclaration != null ) {
			final int theOffset = aAlphabetExpression.indexOf( aAlphabetDeclaration );
			final List<Character> theAlphabet = new ArrayList<>();
			String eSubstring;
			int eEndIndex;
			int eBeginIndex = 0;
			char[] eUtfChars;
			int eCodepoint;
			int eRangeStart;
			int eRangeEnd;
			do {
				eEndIndex = aAlphabetDeclaration.indexOf( EXPRESSION_SEPARATOR, eBeginIndex );
				if ( eEndIndex == -1 ) {
					eEndIndex = aAlphabetDeclaration.length();
				}
				if ( ( eBeginIndex == eEndIndex ) && ( eEndIndex < aAlphabetDeclaration.length() && aAlphabetDeclaration.charAt( eEndIndex ) == EXPRESSION_SEPARATOR ) ) {
					if ( eEndIndex == aAlphabetDeclaration.length() - 1 || ( eEndIndex < aAlphabetDeclaration.length() - 1 && aAlphabetDeclaration.charAt( eEndIndex + 1 ) == EXPRESSION_SEPARATOR ) ) {
						addToAlphabet( theAlphabet, EXPRESSION_SEPARATOR );
						eEndIndex++;
					}
					else {
						throw new ParseException( "Expecting a value at position <" + ( eEndIndex != -1 ? theOffset + eEndIndex : aAlphabetExpression.length() ) + "> of declaration \"" + aAlphabetDeclaration + "\" in expression \"" + aAlphabetExpression + "\"!", theOffset + eBeginIndex );
					}
				}
				else {
					eSubstring = aAlphabetDeclaration.substring( eBeginIndex, eEndIndex );
					if ( eSubstring.length() == 1 ) {
						addToAlphabet( theAlphabet, eSubstring.charAt( 0 ) );
					}
					else if ( eSubstring.length() == 6 && eSubstring.startsWith( UTF_CODEPOINT_PREFIX ) ) {
						try {
							eCodepoint = Integer.parseInt( eSubstring.substring( 2 ), 16 );
							eUtfChars = Character.toChars( eCodepoint );
							for ( char eUtfChar : eUtfChars ) {
								addToAlphabet( theAlphabet, eUtfChar );
							}
						}
						catch ( Exception e ) {
							throw new ParseException( "Unable to parse UTF codepoint from \"" + eSubstring + "\" of expression \"" + aAlphabetDeclaration + "\" at position <" + theOffset + eBeginIndex + ">!", theOffset + eBeginIndex );
						}
					}
					// "a-z" |-->
					else if ( eSubstring.length() == 3 && eSubstring.charAt( 1 ) == LENGTH_SEPARATOR ) {
						eRangeStart = eSubstring.charAt( 0 );
						eRangeEnd = eSubstring.charAt( 2 );
						if ( eRangeStart <= eRangeEnd ) {
							for ( int i = eRangeStart; i <= eRangeEnd; i++ ) {
								addToAlphabet( theAlphabet, (char) i );
							}
						}
						else {
							for ( int i = eRangeStart; i >= eRangeEnd; i-- ) {
								addToAlphabet( theAlphabet, (char) i );
							}
						}
					}
					// "a-z" <--|
					// "U+nnnn" |-->
					else if ( eSubstring.startsWith( UTF_CODEPOINT_PREFIX ) && eSubstring.length() == 13 && eSubstring.charAt( 6 ) == LENGTH_SEPARATOR && eSubstring.indexOf( UTF_CODEPOINT_PREFIX, 7 ) == 7 ) {
						eRangeStart = Integer.parseInt( eSubstring.substring( 2, 6 ), 16 );
						eRangeEnd = Integer.parseInt( eSubstring.substring( 9 ), 16 );
						if ( eRangeStart <= eRangeEnd ) {
							for ( int i = eRangeStart; i <= eRangeEnd; i++ ) {
								addToAlphabet( theAlphabet, (char) i );
							}
						}
						else {
							for ( int i = eRangeStart; i >= eRangeEnd; i-- ) {
								addToAlphabet( theAlphabet, (char) i );
							}
						}
					}
					// "U+nnnn" <--|
					else {
						final CharSet theCharset = CharSet.toCharset( eSubstring );
						if ( theCharset != null ) {
							addToAlphabet( theAlphabet, theCharset.getCharSet() );
						}
						else {
							final EscapeCode theEscape = EscapeCode.toEscapeCode( eSubstring );
							if ( theEscape != null ) {
								addToAlphabet( theAlphabet, theEscape.getEscapeCode() );
							}
							else {
								throw new ParseException( "Expecting a value at position <" + ( eEndIndex != -1 ? theOffset + eEndIndex : aAlphabetExpression.length() ) + "> of declaration \"" + aAlphabetDeclaration + "\" in expression \"" + aAlphabetExpression + "\"!", theOffset + eBeginIndex );
							}
						}
					}
				}
				eBeginIndex = eEndIndex + 1;
			} while ( eEndIndex != -1 && eEndIndex <= aAlphabetDeclaration.length() - 1 );
			_alphabet = new char[theAlphabet.size()];
			for ( int i = 0; i < _alphabet.length; i++ ) {
				_alphabet[i] = theAlphabet.get( i );
			}
		}
	}

	private void parseStartValue( String aStartValueDeclaration, String aAlphabetExpression ) {
		if ( aStartValueDeclaration != null && _alphabet != null ) {
			validateStartValue( aStartValueDeclaration, _minLength, _maxLength, _alphabet );
		}
		_startValue = aStartValueDeclaration;
	}

	private static void addToAlphabet( List<Character> aAlphabet, char... aChars ) {
		for ( char aChar : aChars ) {
			if ( !aAlphabet.contains( aChar ) ) {
				aAlphabet.add( aChar );
			}
		}
	}

	private static String toAlphabetExpression( String aLengthSection, String aAlphabetSection, String aStartValueSection, String aWordsSection, String aWordsFilesSection ) {
		String theExpextedExpression = "";
		if ( aWordsSection != null ) {
			theExpextedExpression += WORDS_BEGIN + aWordsSection + WORDS_END;
		}
		if ( aWordsFilesSection != null ) {
			theExpextedExpression += WORDS_FILES_BEGIN + aWordsFilesSection + WORDS_FILES_END;
		}
		if ( aLengthSection != null ) {
			theExpextedExpression += LENGTH_BEGIN + aLengthSection + LENGTH_END;
		}
		if ( aAlphabetSection != null ) {
			theExpextedExpression += ( theExpextedExpression.length() != 0 ? SECTION_SEPARATOR : "" ) + ALPHABET_BEGIN + aAlphabetSection + ALPHABET_END;
		}
		if ( aStartValueSection != null ) {
			theExpextedExpression += ( theExpextedExpression.length() != 0 ? SECTION_SEPARATOR : "" ) + WORD_BEGIN + aStartValueSection + WORD_END;
		}
		return theExpextedExpression;
	}

	private static String toAltAlphabetExpression( String aLengthSection, String aAlphabetSection, String aStartValueSection, String aWordsSection, String aWordsFilesSection ) {
		String theExpextedExpression = "";
		if ( aWordsSection != null ) {
			theExpextedExpression += WORDS_BEGIN + aWordsSection + WORDS_END;
		}
		if ( aWordsFilesSection != null ) {
			theExpextedExpression += WORDS_FILES_BEGIN + aWordsFilesSection + WORDS_FILES_END;
		}
		if ( aLengthSection != null ) {
			theExpextedExpression += LENGTH_BEGIN + aLengthSection + LENGTH_END;
		}
		if ( aAlphabetSection != null ) {
			theExpextedExpression += ( theExpextedExpression.length() != 0 ? SECTION_SEPARATOR : "" ) + ALPHABET_BEGIN + aAlphabetSection + ALPHABET_END;
		}
		if ( aStartValueSection != null ) {
			theExpextedExpression += ( theExpextedExpression.length() != 0 ? SECTION_SEPARATOR : "" ) + ALT_WORD_BEGIN + aStartValueSection + ALT_WORD_END;
		}
		return theExpextedExpression;
	}

	private static String toSection( String aAlphabetExpression, String aOpenSymbol, String aCloseSymbol, String aSectionSeparator ) throws ParseException {
		final String theOpenSection = aSectionSeparator + aOpenSymbol;
		int theStartIndex = aAlphabetExpression.indexOf( theOpenSection );
		if ( theStartIndex != -1 ) {
			theStartIndex += theOpenSection.length();
		}
		else {
			theStartIndex = aAlphabetExpression.indexOf( aOpenSymbol );
			if ( theStartIndex == 0 ) { // 0!
				theStartIndex += aOpenSymbol.length();
			}
			else {
				return null;
			}
		}
		final String theCloseSection = aCloseSymbol + aSectionSeparator;
		int theEndIndex = aAlphabetExpression.indexOf( theCloseSection, theStartIndex );
		if ( theEndIndex == -1 ) {
			theEndIndex = aAlphabetExpression.lastIndexOf( aCloseSymbol );
			if ( theEndIndex == -1 || theEndIndex != aAlphabetExpression.length() - 1 ) {
				throw new ParseException( "Expecting a closing symbol \"" + aCloseSymbol + "\" at position <" + theStartIndex + "> or beyond (or a missing section separator \"" + aSectionSeparator + "\") for expression \"" + aAlphabetExpression + "\"!", theStartIndex );
			}
		}
		return aAlphabetExpression.substring( theStartIndex, theEndIndex );
	}
}
