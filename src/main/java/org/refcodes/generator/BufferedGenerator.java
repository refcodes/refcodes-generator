// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import org.refcodes.mixin.Suspendable;

/**
 * Base interface for a {@link Generator} using a buffer being filled upon
 * demand or time schedule and which's buffering functionality may be suspended
 * ({@link #suspend()}) so that the {@link Generator} does only emit elements
 * already present in the puffer till all elements in the buffer are expended
 * (and does not place any new elements into the buffer to be emitted).
 *
 * @param <T> the generic type
 */
public interface BufferedGenerator<T> extends Generator<T>, Suspendable {

}
