package org.refcodes.generator;

/**
 * The {@link IdCounterComposite} combines several {@link IdCounter} instances
 * which to use when generating data by daisy chaining the provided generator
 * instances (end of counter 1 increases counter 2, end of counter 2 increases
 * counter 3, ..., end of counter n-1 increases counter n, the end of a counter
 * is determined with {@link #hasNext()} and a counter is increased by
 * {@link #next()}).
 */
public class IdCounterComposite extends AbstractCounterComposite<String> implements IdCounter {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link IdCounterComposite} daisy chaining the provided
	 * {@link IdCounter} instances.
	 * 
	 * @param aCounters The {@link IdCounter} instances to be daisy chained.
	 */
	@SafeVarargs
	public IdCounterComposite( IdCounter... aCounters ) {
		super( aCounters );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String next() {
		final String[] theActualCounters = nextCounters();
		final StringBuilder theBuffer = new StringBuilder();
		for ( String theActualCounter : theActualCounters ) {
			theBuffer.append( theActualCounter );
		}
		return theBuffer.toString();

	}
}
