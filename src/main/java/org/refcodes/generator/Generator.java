// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A {@link Generator} generates objects of a dedicated type in mass production.
 * The generated objects may have https://www.metacodes.pro characteristics or
 * qualities, e.g the objects may be system wide unique from each other. These
 * characteristics are implementation depended. The {@link Iterator} patters has
 * been chosen to make the generator team up with the java collections
 * framework.
 *
 * @param <T> the generic type of the elements being generated.
 */
public interface Generator<T> extends Iterator<T> {

	/**
	 * Tests whether the {@link Generator} is capable of generating a
	 * {@link #next()} item.
	 *
	 * @return true, if successful
	 */
	@Override
	boolean hasNext();

	/**
	 * Generates a next item.
	 * 
	 * @return The next item generated
	 * 
	 * @exception NoSuchElementException in case the generator was not capable
	 *            of generating a next item. Prevent this exception by first
	 *            testing the {@link Generator} using {@link #hasNext()}.
	 */
	@Override
	T next();
}
