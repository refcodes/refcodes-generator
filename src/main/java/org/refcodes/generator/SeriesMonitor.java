// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import org.refcodes.mixin.Clearable;

/**
 * The {@link SeriesMonitor} is a {@link Generator} with means to monitor the
 * last fully processed (completed) ID of the series of generated IDs with all
 * preceding generated IDs also being fully processed (completed): All IDs till
 * the last fully processed (completed) ID have been processed. Any IDs being
 * processed after this last fully processed (completed) ID are not taken into
 * account as between them and the the last fully processed (completed) ID there
 * are gaps of uncompleted (not yet finished) IDs (which were not filled up yet
 * e.g. as of a multi threaded environment).
 *
 * @param <T> the generic type of the elements being generated.
 *
 */
public interface SeriesMonitor<T> extends Generator<T>, Clearable {

	/**
	 * Marks a ID previously generated using {@link #next()} as being processed,
	 * the last ID of fully processed IDs in the series of generated IDs can be
	 * identified using the {@link #lastProcessed()} method.
	 * 
	 * @param aId The ID to be marked as processed.
	 * 
	 * @return True in case the ID has not been marked before as processed,
	 *         false if the ID has never been generated or has already been
	 *         Marked as processed.
	 */
	boolean markAsProcessed( T aId );

	/**
	 * Returns the last ID of fully processed IDs in the series of generated
	 * IDs.
	 * 
	 * @return The according ID or null if nine ID has yet been marked as being
	 *         processed.
	 */
	T lastProcessed();

}
