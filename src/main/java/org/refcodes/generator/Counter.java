// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.generator;

import org.refcodes.mixin.Resetable;

/**
 * A {@link Counter} is a {@link Generator} with means to reset the
 * {@link Counter} state to its initial position (value) from which to begin
 * counting again, e.g. when {@link #hasNext()} returns false, then calling
 * {@link #reset()} will make the {@link Counter} instance to start again from
 * the very first beginning and {@link #hasNext()} will return true until the
 * "end" of the countable elements has been reached (by consuming them with
 * {@link #next()}).
 * 
 * @param <T> the generic type of the elements being generated.
 */
public interface Counter<T> extends Generator<T>, Resetable {}
